#pragma once

#include <Engine\Camera3D.h>

namespace KerpEngine
{
    class Camera3DFirstPerson : public Camera3D
    {
    public:
        Camera3DFirstPerson(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position,
            float farPlane = Camera3D::DEFAULT_FAR_PLANE,
            float nearPlane = Camera3D::DEFAULT_NEAR_PLANE,
            float aspectRatio = Camera3D::DEFAULT_ASPECT_RATIO,
            float fovAngleY = Camera3D::DEFAULT_FOV_ANGLE_Y);
        virtual ~Camera3DFirstPerson();

        void Initialize();

        void Rotate(const float pitch, const float yaw, const float roll);
        void Rotate(const XMFLOAT3 pyr);
    };
}

