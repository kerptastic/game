#pragma once
#include "Image.h"
#include <string>
#include <vector>

namespace KerpEngine
{
		class Bitmap : public Image
		{
		public:
				static const uint16_t BITMAP_FILETYPE = 0x4D42;
				static const std::wstring DEFAULT_FILENAME;
				static const uint32_t DATA_CHUNK_OFFSET;				
				static const uint32_t RED_OFFSET;
				static const uint32_t BLUE_OFFSET;
				static const uint32_t GREEN_OFFSET;

		public:
				Bitmap(const std::wstring filename);
				~Bitmap();
				const uint32_t Height() { return m_height; }
				const uint32_t Width() { return m_width; }
				const std::vector<uint8_t>* Data() { return &m_data; }

#pragma pack(push, 1)
				struct FileHeader {
						uint16_t file_type{ Bitmap::BITMAP_FILETYPE }; // file type - bitmap, a constant
						uint32_t file_size{ 0 };               // size of the file, in bytes
						uint16_t reserved1{ 0 };               // reserved, always 0
						uint16_t reserved2{ 0 };               // reserved, always 0
						uint32_t offset_data{ 0 };             // start position of pixel data
				};

				struct InfoHeader {
						uint32_t size{ 0 };              // size of header in bytes
						int32_t  width{ 0 };             // width in pixels
						int32_t  height{ 0 };            // height in pixels
														//  (neg = top-down, origin top left
														//  (pos = bottum-up, origin bottom left
						uint16_t planes{ 1 };            // num planes for the target device, always 1
						uint16_t bit_count{ 0 };         // num bits per pixel
						uint32_t compression{ 0 };       // 0 or 3 = uncompressed, this only manages uncompressed for now
						uint32_t size_image{ 0 };        // 0 = uncompressed
						int32_t  x_pixels_per_meter{ 0 };
						int32_t  y_pixels_per_meter{ 0 };
						uint32_t colors_used{ 0 };       // num color indexes in the color table, 0 == max
						uint32_t colors_important{ 0 };  // num colors used for displaying the bitmap
				};

				struct ColorHeader {
						uint32_t red_mask{ 0x00ff0000 };         // Bit mask for the red channel
						uint32_t green_mask{ 0x0000ff00 };       // Bit mask for the green channel
						uint32_t blue_mask{ 0x000000ff };        // Bit mask for the blue channel
						uint32_t alpha_mask{ 0xff000000 };       // Bit mask for the alpha channel
						uint32_t color_space_type{ 0x73524742 }; // Default "sRGB" (0x73524742)
						uint32_t unused[16]{ 0 };                // Unused data for sRGB color space
				};
#pragma pack(pop)

		protected:
				void LoadPixelData();
				const bool Read(const std::wstring filename);

		private:
				uint32_t AlignStride(const uint32_t alignment);
				void ResizeHeaders();

				FileHeader  m_fileHeader;
				InfoHeader  m_infoHeader;
				ColorHeader m_colorHeader;

				uint32_t m_rowStride{ 0 };
		};
}