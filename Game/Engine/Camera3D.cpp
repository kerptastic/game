#include "pch.h"
#include "Camera3D.h"

using namespace KerpEngine;
using namespace DirectX;

const float Camera3D::DEFAULT_ASPECT_RATIO = 1.0f;
const float Camera3D::DEFAULT_FAR_PLANE = 2000.0f;
const float Camera3D::DEFAULT_FOV_ANGLE_Y = XM_PIDIV4;
const float Camera3D::DEFAULT_NEAR_PLANE = 0.01f;
const float Camera3D::MAX_PITCH = XM_PIDIV4;

// Initializes a 3D Camera at a given position and lookAt direction.
Camera3D::Camera3D(
    const std::shared_ptr<DX::DeviceResources>& deviceResources,
    const XMFLOAT3 position,
    float farPlane,
    float nearPlane,
    float aspectRatio,
    float fovAngleY) :
    GameObject(deviceResources, position),
    m_farPlane(farPlane),
    m_nearPlane(nearPlane),
    m_aspectRatio(aspectRatio),
    m_fovAngleY(fovAngleY) {

    Initialize();
}

// Public Destructor.
Camera3D::~Camera3D()
{
    m_deviceResources->RegisterDeviceNotify(nullptr);
}

// Initializes the Camera's resources, including view/projection/perspective matrices.
void Camera3D::Initialize()
{
    XMStoreFloat4x4(&m_view, XMMatrixIdentity());
    XMStoreFloat4x4(&m_projection, XMMatrixIdentity());

    XMMATRIX perspectiveMatrix = XMMatrixPerspectiveFovLH(
        m_fovAngleY,
        m_aspectRatio,
        m_nearPlane,
        m_farPlane
    );

    XMFLOAT4X4 orientation = m_deviceResources->GetOrientationTransform3D();
    XMMATRIX orientationMatrix = XMLoadFloat4x4(&orientation);

    //XMStoreFloat4x4(&m_projection, XMMatrixTranspose(perspectiveMatrix * orientationMatrix));
    XMStoreFloat4x4(&m_projection, XMMatrixTranspose(perspectiveMatrix * orientationMatrix));
}

// Reinitializes the View Matrix.
void Camera3D::InitViewMatrix()
{
    XMMATRIX view = XMMatrixTranspose(
        XMMatrixLookAtLH(
            XMLoadFloat3(&m_position),
            XMLoadFloat3(&m_lookAt),
            XMLoadFloat3(&m_up)
        )
    );

    XMStoreFloat4x4(&m_view, view);
}

// Updates the Camera and reinitializes its View Matrix.
void Camera3D::Update(DX::StepTimer const& timer)
{
    GameObject::Update(timer);

    InitViewMatrix();
}

// Prints out Camera diagnostic information
const std::wstring Camera3D::GetDiagText()
{
    std::wstring text =
        L"Position: " + std::to_wstring(m_position.x) +
        L", " + std::to_wstring(m_position.y) +
        L", " + std::to_wstring(m_position.z);

    text +=
        L"\nRotation: " + std::to_wstring(XMConvertToDegrees(m_rotation.x)) +
        L", " + std::to_wstring(XMConvertToDegrees(m_rotation.y)) +
        L", " + std::to_wstring(XMConvertToDegrees(m_rotation.z));

    text +=
        L"\nOrientation: " + std::to_wstring((m_orientation.x)) +
        L", " + std::to_wstring((m_orientation.y)) +
        L", " + std::to_wstring((m_orientation.z));

    text +=
        L"\nTarget: " + std::to_wstring(m_lookAt.x) +
        L", " + std::to_wstring(m_lookAt.y) +
        L", " + std::to_wstring(m_lookAt.z);

    return text;
}