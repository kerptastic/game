#include "pch.h"
#include <WICTextureLoader.h>

#include "TextureManager.h"

#include "..\Common\DirectXHelper.h"

using namespace Microsoft::WRL;
using namespace DirectX;
using namespace KerpEngine;


std::shared_ptr<TextureManager> TextureManager::m_instance(nullptr);
std::once_flag TextureManager::m_flag;

TextureManager::TextureManager(const std::shared_ptr<DX::DeviceResources>& deviceResources) : 
	m_deviceResources(deviceResources)
{
}

//
std::shared_ptr<TextureManager> TextureManager::GetInstance(
	const std::shared_ptr<DX::DeviceResources>& deviceResources)
{
	std::call_once(m_flag, [&]() {
		m_instance.reset(new TextureManager(deviceResources));
	});

	return m_instance;
}

//
ComPtr<ID3D11ShaderResourceView>& TextureManager::GetTexture2D(const std::wstring textureName)
{
	return m_2DtextureMap[textureName];
}

//
void TextureManager::Load2DTexture(const std::wstring textureName)
{
	ComPtr<ID3D11ShaderResourceView> texture;
	ComPtr<ID3D11Resource> resource;

	DX::ThrowIfFailed(
		CreateWICTextureFromFile(
			m_deviceResources->GetD3DDevice(),
			textureName.c_str(),
			resource.GetAddressOf(),
			texture.ReleaseAndGetAddressOf()));

	m_2DtextureMap[L"grass"] = texture;
}

void TextureManager::Load2DTexturesByFile(const std::wstring textureFile)
{
	// TODO: build out textures via file, loop through each file and load into map
	// also, unload and reload textures
}

//
void TextureManager::Reset()
{

}
