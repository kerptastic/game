#pragma once
#include "GameObject.h"

#include <vector>

using namespace DirectX;

namespace KerpEngine
{
    namespace Enums
    {
        enum Face
        {
            FRONT, BACK, TOP, BOTTOM, RIGHT, LEFT
        };

        static const Face FaceValues2D[] = { FRONT, BACK };
        static const Face FaceValues3D[] = { FRONT, BACK, TOP, BOTTOM, RIGHT, LEFT };
    }

    enum IntersectionType
    {
        CONTAINS = 0,
        INTERSECTS = 1,
        NONE = 2
    };

    using namespace KerpEngine::Enums;

    class BoundingVolume : public GameObject
    {
    public:
        BoundingVolume(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position);
        virtual ~BoundingVolume();

        virtual IntersectionType Intersects(const std::shared_ptr<BoundingVolume>& other) = 0;
        virtual IntersectionType Contains(const XMFLOAT3 v) = 0;

        virtual void Update(DX::StepTimer const& timer) = 0;
        virtual void Render(const XMMATRIX view, const XMMATRIX projection) = 0;

    protected:
        virtual const std::vector<XMFLOAT3> GetCollisionFaceCorners(const Face face) { return m_collisionCorners; };

        std::vector<XMFLOAT3> m_collisionCorners;

    private:
    };
}
