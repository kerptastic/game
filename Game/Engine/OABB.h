#pragma once

#include <vector>

#include "BoundingVolume.h"

using namespace KerpEngine::Enums;
using namespace DirectX;

namespace KerpEngine
{
    class OABB : public BoundingVolume
    {
    public:
        OABB(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position,
            const float width,
            const float height,
            const float depth);
        OABB(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 frontTopRight,
            const XMFLOAT3 bottomLeft);
        OABB(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position,
            const XMFLOAT3 frontTopRight,
            const XMFLOAT3 backBottomLeft);
        virtual ~OABB();

        void CreateDeviceDependentResources();
        void CreateWindowSizeDependentResources();

        IntersectionType Intersects(const std::shared_ptr<BoundingVolume>& other);
        IntersectionType Contains(const XMFLOAT3 v);

        void Update(DX::StepTimer const& timer);
        void Render(const XMMATRIX view, const XMMATRIX projection);

    private:
        void Initialize();
        void UpdateCollisionPoints();

        const XMVECTOR GetCollisionFaceNormal(const Face face);

        // origin space vectors for the OABB.
        XMFLOAT3 m_frontTopRight, m_frontBottomRight, m_frontBottomLeft, m_frontTopLeft;
        XMFLOAT3 m_backTopRight, m_backBottomRight, m_backBottomLeft, m_backTopLeft;

        // translated world space collision vectors for the OABB.
        XMFLOAT3 m_collisionFrontTopRight, m_collisionFrontBottomRight, m_collisionFrontBottomLeft, m_collisionFrontTopLeft;
        XMFLOAT3 m_collisionBackTopRight, m_collisionBackBottomRight, m_collisionBackBottomLeft, m_collisionBackTopLeft;

        // OABB width.
        float m_width;
        // OABB height.
        float m_height;
        // OABB depth.
        float m_depth;
    };
}