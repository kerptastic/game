#include "pch.h"
#include "Terrain3D.h"

#include "..\Common\DirectXHelper.h"
#include "Bitmap.h"
#include "EngineUtils.h"

using namespace KerpEngine;

using namespace DirectX;
using namespace Windows::Foundation;

const float Terrain3D::DEFAULT_MIN_HEIGHT = -50.0f;
const float Terrain3D::DEFAULT_MAX_HEIGHT = 50.0f;

// Initializes the trerrain with the given position and terrain data file
Terrain3D::Terrain3D(
    const std::shared_ptr<DX::DeviceResources>& deviceResources,
    const XMFLOAT3 position,
    ComPtr<ID3D11ShaderResourceView>& baseTexture,
    const std::shared_ptr<Image> heightMapImage,
    float minHeight,
    float maxHeight) :
    GameObject(deviceResources, position), m_heightMapImage(heightMapImage), m_baseTexture(baseTexture), m_minHeight(minHeight), m_maxHeight(maxHeight) {

    Initialize();
}

// Default destructor.
Terrain3D::~Terrain3D() {

}

//
void Terrain3D::Initialize() {
    GenerateHeightMapData();
    GenerateMeshData();

    CreateDeviceDependentResources();
    CreateWindowSizeDependentResources();
}

//
void Terrain3D::GenerateHeightMapData() {
    uint32_t rows = Height();
    uint32_t cols = Width();

    XMFLOAT3 default3f = { 0.0f, 0.0f, 0.0f };
    XMFLOAT2 default2f = { 0.0f, 0.0f };

    Vertex vert;

    const std::vector<Image::Pixel>* pixelData = m_heightMapImage->Pixels();
    uint32_t row, col;

    for (uint32_t p = 0; p < pixelData->size(); p++) {
        Image::Pixel pixel = pixelData->at(p);
        row = pixel.row;
        col = pixel.column;

        float vHeight = CalculateHeight(pixel.red, pixel.green, pixel.blue);
        //float vHeight = 0.0f;

        vert = {
            { (float)row, vHeight, (float)col },
            { (float)pixel.red / 255.0f, (float)pixel.green / 255.0f, (float)pixel.blue / 255.0f },
            default3f,
            default2f,
        };

        m_heightMapVertices.push_back(vert);

        // going to skip the last row, and the last column as we are collecting indices
        // from left to right, top to bottom - therefore the far right column, and bottom
        // row will have no triangles to the right, and below them, respectively
        //
        //  0---1---2  [Row,Col] [0,0] = {0,3,1} & {1,3,4}
        //  | / | / |            [0,1] = {1,4,2} & {2,4,5}
        //  3---4---5            [0,2] = skip
        //  | / | / |            etc
        //  6---7---8            [2,0], [2,1], [2,2] = skip

        if (col < cols - 1 && row < rows - 1) {
            // define a square worth of indices (2x triangles) and add them to the array
            int32_t tl = (row * cols) + col;
            int32_t tr = (row * cols) + col + 1;

            int32_t bl = ((row + 1) * cols) + col;
            int32_t br = ((row + 1) * cols) + col + 1;

            // clockwise winding
            m_heightMapIndices.push_back(bl);
            m_heightMapIndices.push_back(tl);
            m_heightMapIndices.push_back(tr);

            m_heightMapIndices.push_back(bl);
            m_heightMapIndices.push_back(tr);
            m_heightMapIndices.push_back(br);

            // add the triangle data for later processing of normals
            //m_modelTriangles.push_back({ br, tr, bl, defaultNormal });
            //m_modelTriangles.push_back({ bl, tr, tl, defaultNormal });

            // we need to take vertices from each of the two triangles drawn in order to get a normal
            // for the face. since counterclockwise, we grab top left, bottom left, bottom right

            //  TL-----TR   Tri 1) BR, TR, BL
            //  |     / |   Tri 2) BL, TR, TL (see above)
            //  |   /   |
            //  | /     |   Face ) BR, BL, TL (clockwise, both triangles included)
            //  BL-----BR
            m_heightMapFaces.push_back({ tl, tr, bl, br, 0.0f, 0.0f, default3f });
        }
    }
}

void Terrain3D::GenerateMeshData() {
    Vertex vert1, vert2, vert3, vert4;
    XMVECTOR vVert1, vVert2, vVert3, normal;
    FaceNormalIndexed* face;

    // first get the heightmap face normals
    for (uint32_t t = 0; t < m_heightMapFaces.size(); t++) {
        face = &m_heightMapFaces.at(t);
        vert1 = m_heightMapVertices.at(face->br);
        vert2 = m_heightMapVertices.at(face->bl);
        vert3 = m_heightMapVertices.at(face->tl);
        vert4 = m_heightMapVertices.at(face->tr);

        // only need 3 verts for the normal
        vVert1 = XMLoadFloat3(&vert1.position);
        vVert2 = XMLoadFloat3(&vert2.position);
        vVert3 = XMLoadFloat3(&vert3.position);

        normal = EngineUtils::GetSurfaceNormal(vVert1, vVert2, vVert3);
        XMStoreFloat3(&face->normal, normal);
    }

    // now load up the vertex/index data for the actual mesh - we can then unload the height
    // map vertices when we are done, as we will need to duplicate some vertices to get
    // 2 normals for each shared vertex between faces

    Vertex hmvData1, hmvData2, hmvData3;

    // length minus 3 since we are pulling 3 indices off at a time, with our step being +=3 also
    for (uint32_t i = 0; i < m_heightMapIndices.size(); i += 3) {
        hmvData1 = m_heightMapVertices.at(m_heightMapIndices.at(i));
        hmvData2 = m_heightMapVertices.at(m_heightMapIndices.at(i + 1));
        hmvData3 = m_heightMapVertices.at(m_heightMapIndices.at(i + 2));

        // grab the mesh index that points to the vertex we want the normal for and pass it
        // to the generate vertex normal function, which will be used to calculate the row/col
        // to grab the appropriate model face from
        vert1.position = hmvData1.position;
        vert1.texture0 = { 0.0f, 1.0f };
        XMStoreFloat3(&vert1.normal, GenerateVertexNormal(m_heightMapIndices.at(i)));
        m_meshVertices.push_back(vert1);
        m_meshIndices.push_back(i);

        vert2.position = hmvData2.position;
        vert2.texture0 = { (i + 1) % 2 != 0 ? 0.0f : 1.0f, 0.0f };
        XMStoreFloat3(&vert2.normal, GenerateVertexNormal(m_heightMapIndices.at(i + 1)));
        m_meshVertices.push_back(vert2);
        m_meshIndices.push_back(i + 1);

        vert3.position = hmvData3.position;
        vert3.texture0 = { 1.0f, (i + 2) % 2 == 0 ? 0.0f : 1.0f };
        XMStoreFloat3(&vert3.normal, GenerateVertexNormal(m_heightMapIndices.at(i + 2)));
        m_meshVertices.push_back(vert3);
        m_meshIndices.push_back(i + 2);

        // used the data below to determine the texture coordinates based on how the indices were
        // added to the vector. The 2nd and 3rd vertices in each pass vary, and swap between odd 
        // and even values. Used modulus to flip the bit accordingly.

        //m_heightMapIndices.push_back(bl);  0,1  (0, 1)  -  0  6
        //m_heightMapIndices.push_back(tl);  0,0  (?, 0)  -  1  7  -  (i + 1) % 2 != 0 ? 0.0f : 1.0f, 0.0f
        //m_heightMapIndices.push_back(tr);  1,0  (1, ?)  -  2  8  -  1.0f, (i + 2) % 2 == 0 ? 0.0f : 1.0f

        //m_heightMapIndices.push_back(bl);  0,1  (0, 1)  -  3  9
        //m_heightMapIndices.push_back(tr);  1,0  (?, 0)  -  4 10  -  (i + 1) % 2 != 0 ? 0.0f : 1.0f, 0.0f
        //m_heightMapIndices.push_back(br);  1,1  (1, ?)  -  5 11  -  1.0f, (i + 2) % 2 == 0 ? 0.0f : 1.0f
    }
}

//
const float Terrain3D::GetHeightAt(float x, float z) {
    float result = 0.0f;

    if (x < 0.0f || z < 0.0f) {
        return result;
    }

    uint32_t floorX = (uint32_t)x;
    uint32_t floorZ = (uint32_t)z;

    uint32_t index = (floorX * (Width() - 1)) + (floorZ);
    FaceNormalIndexed face;

    if (index > 0 && index < m_heightMapFaces.size()) {
        face = m_heightMapFaces.at(index);

        result = EngineUtils::BilinearInterpolateF(
            x, z,
            m_heightMapVertices.at(face.tl),
            m_heightMapVertices.at(face.tr),
            m_heightMapVertices.at(face.bl),
            m_heightMapVertices.at(face.br));

    }

    return result;
    //return result * (m_scale.y == 0.0f ? 1.0f : m_scale.y);
}

//
XMVECTOR Terrain3D::GenerateVertexNormal(uint32_t index) {
    FaceNormalIndexed* face;
    XMFLOAT3 summedNormal = { 0.0f, 0.0f, 0.0f };
    uint32_t row = index / Width();
    uint32_t col = index - (row * Width());

    // cols = Width() and rows = Height()
    // left wall  : v % cols == 0         ~    v % cols != 0
    // right wall : v+1 % cols == 0       ~    v+1 % cols != 0
    // top wall   : v < cols              ~    v >= cols
    // bottom wall: v >= (rows-1) * cols   ~    v < (rows-1) * cols

    // top-left face normal - if on top or left edge, ignore
    if (index >= Width() && index % Width() != 0) {
        face = &m_heightMapFaces.at(index - (Width() + row));
        summedNormal.x += face->normal.x;
        summedNormal.y += face->normal.y;
        summedNormal.z += face->normal.z;
    }

    // top right face normal - if on top or right edge, ignore
    if (index >= Width() && (index + 1) % Width() != 0) {
        face = &m_heightMapFaces.at(index - (Width() + row - 1));
        summedNormal.x += face->normal.x;
        summedNormal.y += face->normal.y;
        summedNormal.z += face->normal.z;
    }

    // bottom left face normal - if on bottom or left edge, ignore
    if (index < (Height() - 1) * Width() && index % Width() != 0) {
        face = &m_heightMapFaces.at(index - (row + 1));
        summedNormal.x += face->normal.x;
        summedNormal.y += face->normal.y;
        summedNormal.z += face->normal.z;
    }

    // bottom right face normal - if on bottom or right edge, ignore
    if (index < (Height() - 1) * Width() && (index + 1) % Width() != 0) {
        face = &m_heightMapFaces.at(index - row);
        summedNormal.x += face->normal.x;
        summedNormal.y += face->normal.y;
        summedNormal.z += face->normal.z;
    }

    return DirectX::XMVector3Normalize(XMLoadFloat3(&summedNormal));
}

//
float Terrain3D::CalculateHeight(const uint8_t r, const uint8_t g, const uint8_t b, const float maxPixelColor) {
    float rgb = (float)(((0xFF & r) << 16) | ((0xFF & g) << 8) | ((0xFF & b)));

    return m_minHeight + abs(m_maxHeight - m_minHeight) * (rgb / maxPixelColor);
}

//
void Terrain3D::Update(DX::StepTimer const& timer) {
    GameObject::Update(timer);
}

//
void Terrain3D::Render(const XMMATRIX view, const XMMATRIX projection) {
    // Loading is asynchronous. Only draw geometry after it's loaded.
    if (!m_loadingComplete)
    {
        return;
    }

    auto context = m_deviceResources->GetD3DDeviceContext();

    if (m_isWireframe) {
        context->RSSetState(m_wireframeRasterizerState.Get());
    }
    else {
        context->RSSetState(m_solidRasterizerState.Get());
    }

    XMMATRIX model =
        XMMatrixTranspose(XMMatrixScalingFromVector(XMLoadFloat3(&m_scale))) *
        XMMatrixTranspose(XMMatrixTranslationFromVector(XMLoadFloat3(&m_position))) *
        XMMatrixTranspose(XMMatrixRotationRollPitchYawFromVector(XMLoadFloat3(&m_rotation)));

    // Prepare to pass the updated model matrix to the shader
    XMStoreFloat4x4(&m_constantBufferData.model, model);
    XMStoreFloat4x4(&m_constantBufferData.view, view);
    XMStoreFloat4x4(&m_constantBufferData.projection, projection);

    // Each vertex is one instance of the Vertex struct.
    UINT stride = sizeof(Vertex);
    UINT offset = 0;
    context->IASetVertexBuffers(0, 1, m_vertexBuffer.GetAddressOf(), &stride, &offset);
    context->IASetIndexBuffer(m_indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
    context->IASetInputLayout(m_inputLayout.Get());
    context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    // Setup lighting
    //m_constantBufferData.lightDirection = { 0.0f, -1.0f, 0.0f, 1.0f };
    m_constantBufferData.lightDirection = { -0.577f, -0.577f, -0.577f, 1.0f };
    m_constantBufferData.lightColor = { 1.0f, 1.0f, 0.0f, 1.0f };
    m_constantBufferData.outputColor = { 0.0f, 0.0f, 0.0f, 1.0f };


    // Prepare the constant buffer to send it to the graphics device.
    //context->UpdateSubresource1(m_constantBuffer.Get(), 0, NULL, &m_constantBufferData, 0, 0, 0);
    {
        D3D11_MAPPED_SUBRESOURCE mapped;
        context->Map(m_constantBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped);
        memcpy(mapped.pData, &m_constantBufferData, sizeof(ModelViewProjectionConstantBuffer));
        context->Unmap(m_constantBuffer.Get(), 0);
    }

    // Attach our vertex shader.
    context->VSSetShader(m_vertexShader.Get(), nullptr, 0);

    // Send the constant buffer to the graphics device.
    context->VSSetConstantBuffers(0, 1, m_constantBuffer.GetAddressOf());

    // Attach our pixel shader.
    context->PSSetShader(m_pixelShader.Get(), nullptr, 0);
    context->PSSetShaderResources(0, 1, m_baseTexture.GetAddressOf());
    context->PSSetSamplers(0, 1, m_sampler.GetAddressOf());
    context->PSSetConstantBuffers(0, 1, m_constantBuffer.GetAddressOf());

    // Draw the terrain.
    context->DrawIndexed(m_meshIndices.size(), 0, 0);

    // TODO: make rendering the bounding volume optional as part of debug mode
    //m_boundingVolume->Render(view, projection);
}

//
void Terrain3D::CreateDeviceDependentResources() {
    // Load shaders asynchronously.
    auto loadVSTask = DX::ReadDataAsync(L"DefaultVertexShader.cso");
    auto loadPSTask = DX::ReadDataAsync(L"DefaultPixelShader.cso");
    auto loadTDTask = DX::ReadDataAsync(L"Content\\grass_1024x1024.jpg");

    auto createVSTask = loadVSTask.then([this](const std::vector<byte>& fileData) {
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateVertexShader(&fileData[0], fileData.size(), nullptr, &m_vertexShader)
        );

        static const D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        };

        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateInputLayout(vertexDesc, ARRAYSIZE(vertexDesc), &fileData[0], fileData.size(), &m_inputLayout)
        );
    });

    // After the pixel shader file is loaded, create the shader and constant buffer.
    auto createPSTask = loadPSTask.then([this](const std::vector<byte>& fileData) {
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreatePixelShader(&fileData[0], fileData.size(), nullptr, &m_pixelShader)
        );

        CD3D11_BUFFER_DESC constantBufferDesc(sizeof(ModelViewProjectionConstantBuffer), D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE);
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateBuffer(&constantBufferDesc, nullptr, &m_constantBuffer)
        );
    });

    auto createTDTask = loadTDTask.then([this](const std::vector<byte>& vertexShaderBytecode) {
        D3D11_SAMPLER_DESC samplerDesc;
        ZeroMemory(&samplerDesc, sizeof(samplerDesc));

        samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

        // The sampler does not use anisotropic filtering, so this parameter is ignored.
        samplerDesc.MaxAnisotropy = 0;

        // Specify how texture coordinates outside of the range 0..1 are resolved.
        samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
        samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
        samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

        // Use no special MIP clamping or bias.
        samplerDesc.MipLODBias = 0.0f;
        samplerDesc.MinLOD = 0;
        samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

        // Don't use a comparison function.
        samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;

        // Border address mode is not used, so this parameter is ignored.
        samplerDesc.BorderColor[0] = 0.0f;
        samplerDesc.BorderColor[1] = 0.0f;
        samplerDesc.BorderColor[2] = 0.0f;
        samplerDesc.BorderColor[3] = 0.0f;

        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateSamplerState(&samplerDesc, &m_sampler)
        );
    });

    // 
    auto createMeshTask = (createPSTask && createVSTask).then([this]() {
        D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
        vertexBufferData.pSysMem = m_meshVertices.data();
        vertexBufferData.SysMemPitch = 0;
        vertexBufferData.SysMemSlicePitch = 0;

        CD3D11_BUFFER_DESC vertexBufferDesc(sizeof(Vertex) * m_meshVertices.size(), D3D11_BIND_VERTEX_BUFFER);
        vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
        vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
        vertexBufferDesc.MiscFlags = 0;
        vertexBufferDesc.StructureByteStride = 0;

        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &m_vertexBuffer)
        );

        // setup triangle index buffer
        D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
        indexBufferData.pSysMem = m_meshIndices.data();
        indexBufferData.SysMemPitch = 0;
        indexBufferData.SysMemSlicePitch = 0;

        CD3D11_BUFFER_DESC indexBufferDesc(sizeof(uint32_t) * m_meshIndices.size(), D3D11_BIND_INDEX_BUFFER);
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &m_indexBuffer)
        );
    });

    createMeshTask.then([this]() {
        m_loadingComplete = true;

        // loading is complete, clear out the unneeded heightmap information
        //m_heightMapVertices.clear();
        //m_heightMapFaces.clear();
    });
}

//
void Terrain3D::CreateWindowSizeDependentResources() {

}

//
void Terrain3D::ReleaseDeviceDependentResources() {
    GameObject::ReleaseDeviceDependentResources();

}
