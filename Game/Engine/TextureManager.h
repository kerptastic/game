#pragma once

#include <map>
#include <mutex>
#include "..\Common\DeviceResources.h"

using namespace Microsoft::WRL;

namespace KerpEngine
{
	class TextureManager
	{
	public:
		~TextureManager() {};

		static std::shared_ptr<TextureManager> GetInstance(
			const std::shared_ptr<DX::DeviceResources>& deviceResources);

		ComPtr<ID3D11ShaderResourceView>& GetTexture2D(const std::wstring textureName);

		void Load2DTexture(const std::wstring textureName);
		void Load2DTexturesByFile(const std::wstring textureFile);
		void Reset();

	protected:

	private:
		TextureManager(const std::shared_ptr<DX::DeviceResources>& deviceResources);

		static std::shared_ptr<TextureManager> m_instance;
		static std::once_flag m_flag;

		const std::shared_ptr<DX::DeviceResources>& m_deviceResources;
		std::map<std::wstring, ComPtr<ID3D11ShaderResourceView>> m_2DtextureMap;

	};
}
