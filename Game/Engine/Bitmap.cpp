#include "pch.h"
#include "Bitmap.h"
#include <fstream>

using namespace KerpEngine;

const std::wstring Bitmap::DEFAULT_FILENAME = L"Content\\default.bmp";
const uint32_t Bitmap::DATA_CHUNK_OFFSET = 3;
const uint32_t Bitmap::RED_OFFSET = 2;
const uint32_t Bitmap::BLUE_OFFSET = 1;
const uint32_t Bitmap::GREEN_OFFSET = 0;

Bitmap::Bitmap(const std::wstring filename) : Image(filename)
{
    bool success = true;

    if (!this->Read(filename))
    {
        // TODO: initial bitmap load failed, using default

        if (!this->Read(DEFAULT_FILENAME))
        {
            // TODO: debugging message trying to load default
            // TODO: big problem here if the default image didnt load
            success = false;
        }
    }

    if (success) {
        LoadPixelData();
    }
}

Bitmap::~Bitmap()
{

}

// 
const bool Bitmap::Read(const std::wstring filename)
{
    m_filename = filename;

    std::ifstream bmpFS{ m_filename.c_str(), std::ios_base::binary };

    if (!bmpFS) {
        // TODO: debugging message
        return false;
    }

    bmpFS.read((char*)& m_fileHeader, sizeof(m_fileHeader));

    if (this->m_fileHeader.file_type != Bitmap::BITMAP_FILETYPE)
    {
        // TODO: debugging message
        return false;
    }

    bmpFS.read((char*)& m_infoHeader, sizeof(m_infoHeader));

    // check for transparancy to see if you need to use the color header
    if (m_infoHeader.bit_count == 32)
    {
        // checking for bit mask color information - fail if the data is missing, corrupt file
        if (m_infoHeader.size < (sizeof(InfoHeader) + sizeof(ColorHeader)))
        {
            // TODO: debugging message
            return false;
        }

        bmpFS.read((char*)& m_colorHeader, sizeof(m_colorHeader));

        // Check if the pixel data is stored as BGRA and if the color space type is sRGB
        // TODO CheckColorHeader();
    }

    // load the pixel data
    bmpFS.seekg(m_fileHeader.offset_data, bmpFS.beg);

    ResizeHeaders();

    if (m_infoHeader.height < 0) {
        // TODO: this only handled bottom left corner
    }
    else
    {
        m_data.resize(m_infoHeader.width * m_infoHeader.height * (m_infoHeader.bit_count / 8));

        if (m_infoHeader.width % 4 == 0) {
            bmpFS.read((char*)m_data.data(), m_data.size());
            m_fileHeader.file_size += m_data.size();
        }
        else {
            m_rowStride = m_infoHeader.width * m_infoHeader.bit_count / 8;
            uint32_t newStride = AlignStride(4);
            std::vector<uint8_t> paddingRow(newStride - m_rowStride);

            for (int h = 0; h < m_infoHeader.height; ++h) {
                bmpFS.read((char*)(m_data.data() + m_rowStride * h), m_rowStride);
                bmpFS.read((char*)paddingRow.data(), paddingRow.size());
            }

            m_fileHeader.file_size += m_data.size() + m_infoHeader.height * paddingRow.size();
        }
    }

    m_width = m_infoHeader.width;
    m_height = m_infoHeader.height;

    return true;
}

void Bitmap::LoadPixelData() {
    // initializing the alpha channel to 1 since we will never care about it in a bitmap
    uint8_t dataR, dataG, dataB, dataA = 1;
    uint32_t row, col;

    for (uint32_t p = 0; p < m_data.size(); p += DATA_CHUNK_OFFSET) {
        dataR = m_data.at(p + RED_OFFSET);
        dataG = m_data.at(p + BLUE_OFFSET);
        dataB = m_data.at(p + GREEN_OFFSET);

        row = (p / DATA_CHUNK_OFFSET) / m_width;
        col = (p / DATA_CHUNK_OFFSET) - (row * m_width);


        m_pixelData.push_back({ dataR, dataG, dataB, dataA, row, col });
    }

}
// 
void Bitmap::ResizeHeaders() {
    if (m_infoHeader.bit_count == 32) {
        m_infoHeader.size = sizeof(InfoHeader) + sizeof(ColorHeader);
        m_fileHeader.offset_data = sizeof(FileHeader) + sizeof(InfoHeader) + sizeof(ColorHeader);
    }
    else {
        m_infoHeader.size = sizeof(InfoHeader);
        m_fileHeader.offset_data = sizeof(FileHeader) + sizeof(InfoHeader);
    }

    m_fileHeader.file_size = m_fileHeader.offset_data;
}

// 
uint32_t Bitmap::AlignStride(const uint32_t alignment) {
    uint32_t newStride = m_rowStride;

    while (newStride % alignment != 0) {
        newStride++;
    }

    return newStride;
}
