#pragma once

#include "BoundingVolume.h"

#include <vector>

using namespace KerpEngine::Enums;
using namespace DirectX;

namespace KerpEngine
{
    class AABB : public BoundingVolume
    {
    public:
        AABB(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position,
            const float width,
            const float height);
        AABB(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 topRight,
            const XMFLOAT3 bottomLeft);
        AABB(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position,
            const XMFLOAT3 topRight,
            const XMFLOAT3 bottomLeft);
        virtual ~AABB();

        void CreateDeviceDependentResources();
        void CreateWindowSizeDependentResources();

        IntersectionType Intersects(const std::shared_ptr<BoundingVolume>& other);
        IntersectionType Contains(const XMFLOAT3 v);

        void Update(DX::StepTimer const& timer);
        void Render(const XMMATRIX view, const XMMATRIX projection);

    private:
        void Initialize();
        void UpdateCollisionPoints();

        const XMVECTOR GetCollisionFaceNormal(const Face face);

        // origin space vectors for the AABB.
        XMFLOAT3 m_topRight, m_topLeft;
        XMFLOAT3 m_bottomRight, m_bottomLeft;

        // translated world space collision vectors for the AABB.
        XMFLOAT3 m_collisionTopRight, m_collisionTopLeft, m_collisionBottomRight, m_collisionBottomLeft;

        // AABB width.
        float m_width;
        // AABB height.
        float m_height;
    };
}