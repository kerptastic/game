#pragma once

using namespace DirectX;

namespace KerpEngine
{
    class EngineUtils
    {
    public:
        static const float EngineUtils::BilinearInterpolateF(float x, float z,
            Vertex tl, Vertex tr, Vertex bl, Vertex br) {

            float result = 0.0f;

            float deltaXTL = x - tr.position.x;
            float deltaXBL = bl.position.x - x;

            float deltaZTR = tr.position.z - z;
            float deltaZTL = z - tl.position.z;

            float deltaX = bl.position.x - tl.position.x;
            float deltaZ = tr.position.z - tl.position.z;

            result = 1.0f / (deltaX * deltaZ) * (
                (tl.position.y * deltaZTR * deltaXBL) +
                (bl.position.y * deltaZTR * deltaXTL) +
                (tr.position.y * deltaZTL * deltaXBL) +
                (br.position.y * deltaZTL * deltaXTL)
                );

            return result;
        }

        static const XMVECTOR EngineUtils::GetSurfaceNormal(const XMVECTOR vert1, const XMVECTOR vert2, const XMVECTOR vert3) {
            return XMVector3Normalize(XMVector3Cross(vert1 - vert3, vert2 - vert3));
        }

        static void EngineUtils::BoundRotation(XMFLOAT3& rotation) {
            if (rotation.x >= XM_2PI) {
                rotation.x -= XM_2PI;
            }
            if (rotation.x < 0.0f) {
                rotation.x += XM_2PI;
            }

            if (rotation.y >= XM_2PI) {
                rotation.y -= XM_2PI;
            }
            if (rotation.y < 0.0f) {
                rotation.y += XM_2PI;
            }

            if (rotation.z >= XM_2PI) {
                rotation.z -= XM_2PI;
            }
            if (rotation.z < 0.0f) {
                rotation.z += XM_2PI;
            }
        }
    };
};
