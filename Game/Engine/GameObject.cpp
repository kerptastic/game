#include "pch.h"
#include <math.h>

#include "GameObject.h"
#include "BoundingVolume.h"
#include "EngineUtils.h"

#include "..\Common\DirectXHelper.h"

using namespace KerpEngine;

using namespace DirectX;
using namespace Windows::Foundation;

// Loads vertex and pixel shaders from files and instantiates geometry, while initializing the
// starting position and lookAt directions.
GameObject::GameObject(
    const std::shared_ptr<DX::DeviceResources>& deviceResources,
    const XMFLOAT3 position) :
    m_loadingComplete(false),
    m_indexCount(0),
    m_position(position),
    m_scale({ 1.0f, 1.0f, 1.0f }),
    m_rotation({ 0.0f, 0.0f, 0.0f }),
    m_lookAt({ 0.0f, 0.0f, 1.0f }),
    m_referenceLookAt({ 0.0f, 0.0f, 1.0f }),
    m_up({ 0.0f, 1.0f, 0.0f }),
    m_deviceResources(deviceResources)
{
    Initialize();
}

// Intializes the GameObject.
void GameObject::Initialize()
{
    // setup rasterizers
    ZeroMemory(&m_wireframeRasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
    m_wireframeRasterizerDesc.CullMode = D3D11_CULL_BACK;
    m_wireframeRasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
    m_wireframeRasterizerDesc.FrontCounterClockwise = false;
    m_deviceResources->GetD3DDevice()->CreateRasterizerState(&m_wireframeRasterizerDesc, m_wireframeRasterizerState.GetAddressOf());

    ZeroMemory(&m_solidRasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
    m_solidRasterizerDesc.CullMode = D3D11_CULL_BACK;
    m_solidRasterizerDesc.FillMode = D3D11_FILL_SOLID;
    m_solidRasterizerDesc.FrontCounterClockwise = false;
    m_deviceResources->GetD3DDevice()->CreateRasterizerState(&m_solidRasterizerDesc, m_solidRasterizerState.GetAddressOf());

    // set to solid by default
    m_deviceResources->GetD3DDeviceContext()->RSSetState(m_solidRasterizerState.Get());
}

// Moves the Object in an x/y/z direction, which will be in reference to the current lookAt vector.
void GameObject::Move(const XMFLOAT3 amount)
{
    XMMATRIX ori = XMMatrixRotationQuaternion(XMLoadFloat4(&m_orientation));
    XMVECTOR oriAmount = XMVector3Transform(XMLoadFloat3(&amount), ori);

    XMFLOAT3 realAmount;
    XMStoreFloat3(&realAmount, oriAmount);

    m_position.x += realAmount.x;
    m_position.y += realAmount.y;
    m_position.z += realAmount.z;

    if (m_boundingVolume) {
        m_boundingVolume->SetPosition(m_position);
    }
}

// Rotates the Object the desired pitch/yaw/roll increment, which will update the lookAt and Up vectors.
void GameObject::Rotate(const XMFLOAT3 pyr)
{
    m_rotation.x += pyr.x;
    m_rotation.y += pyr.y;
    m_rotation.z += pyr.z;

    EngineUtils::BoundRotation(m_rotation);

    XMStoreFloat4(&m_orientation, XMQuaternionRotationRollPitchYaw(m_rotation.x, m_rotation.y, m_rotation.z));

    if (m_boundingVolume) {
        m_boundingVolume->SetRotation(m_rotation);
    }
}

// Scales the Object the desired amount in the x/y/z directions.
void GameObject::Scale(const XMFLOAT3 scale)
{
    m_scale = scale;

    if (m_boundingVolume) {
        m_boundingVolume->SetScale(m_scale);
    }
}

// Updates the Object's lookAt direction based on its position and rotation.
void GameObject::Update(DX::StepTimer const& timer)
{
    XMVECTOR transReference = XMVector3Transform(
        XMLoadFloat3(&m_referenceLookAt),
        XMMatrixRotationQuaternion(XMLoadFloat4(&m_orientation)));

    XMFLOAT3 transReferenceF;
    XMStoreFloat3(&transReferenceF, transReference);

    m_lookAt = {
        m_position.x + transReferenceF.x,
        m_position.y + transReferenceF.y,
        m_position.z + transReferenceF.z
    };

    // update the bounding volume
    if (m_boundingVolume) {
        m_boundingVolume->Update(timer);
    }
}

// Release all device resources.
void GameObject::ReleaseDeviceDependentResources()
{
    m_loadingComplete = false;
    m_vertexShader.Reset();
    m_inputLayout.Reset();
    m_pixelShader.Reset();
    m_constantBuffer.Reset();
    m_vertexBuffer.Reset();
    m_indexBuffer.Reset();

    m_wireframeRasterizerState.Reset();
    m_solidRasterizerState.Reset();
}

