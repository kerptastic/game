#include "pch.h"
#include "Camera3DFirstPerson.h"

using namespace KerpEngine;

//
Camera3DFirstPerson::Camera3DFirstPerson(
    const std::shared_ptr<DX::DeviceResources>& deviceResources,
    const XMFLOAT3 position,
    float farPlane,
    float nearPlane,
    float aspectRatio,
    float fovAngleY) :
    Camera3D(deviceResources, position, farPlane, nearPlane, aspectRatio, fovAngleY)
{
}

//
Camera3DFirstPerson::~Camera3DFirstPerson()
{
}

//
void Camera3DFirstPerson::Initialize()
{

}

// Overridden rotation, takes individual pitch yaw and roll values.
void Camera3DFirstPerson::Rotate(const float pitch, const float yaw, const float roll)
{
    Rotate({ pitch, yaw, roll });
}

// Overridden rotation, which will pin the camera's yaw to not go past 90 degrees up/down.
void Camera3DFirstPerson::Rotate(const XMFLOAT3 pyr)
{
    m_rotation.x += pyr.x;
    m_rotation.y += pyr.y;
    m_rotation.z += pyr.z;

    // lock the camera to not be able to pitch past 90* up or down
    m_rotation.x = __min(XM_PIDIV2, m_rotation.x);
    m_rotation.x = __max(-XM_PIDIV2, m_rotation.x);

    if (m_rotation.y >= XM_2PI) {
        m_rotation.y -= XM_2PI;
    }
    if (m_rotation.y < 0.0f) {
        m_rotation.y += XM_2PI;
    }

    if (m_rotation.z >= XM_2PI) {
        m_rotation.z -= XM_2PI;
    }
    if (m_rotation.z < 0.0f) {
        m_rotation.z += XM_2PI;
    }

    XMStoreFloat4(&m_orientation, XMQuaternionRotationRollPitchYaw(m_rotation.x, m_rotation.y, m_rotation.z));
}
