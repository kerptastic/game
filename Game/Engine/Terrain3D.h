#pragma once

#include "GameObject.h"
#include "Image.h"
#include "..\Common\DeviceResources.h"
#include <string>

using namespace DirectX;

namespace KerpEngine
{
    class Terrain3D : public GameObject
    {
    public:
        static const float DEFAULT_MIN_HEIGHT;
        static const float DEFAULT_MAX_HEIGHT;
    public:
        Terrain3D(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position,
            ComPtr<ID3D11ShaderResourceView>& baseTexture,
            const std::shared_ptr<Image> heightMapImage,
            float minHeight = DEFAULT_MIN_HEIGHT,
            float maxHeight = DEFAULT_MAX_HEIGHT);
        ~Terrain3D();

        void Update(DX::StepTimer const& timer);
        void Render(const XMMATRIX view, const XMMATRIX projection);
        void CreateDeviceDependentResources();
        void CreateWindowSizeDependentResources();
        void ReleaseDeviceDependentResources();

        const float MinHeight() { return m_minHeight; }
        const float MaxHeight() { return m_maxHeight; }

        const uint32_t Width() { return m_heightMapImage->Width(); }
        const uint32_t Height() { return m_heightMapImage->Height(); }

        const std::vector<Vertex>* HeightMapVertices() { return &m_heightMapVertices; };
        const float GetHeightAt(float x, float z);

        void ToggleWireframe() { m_isWireframe = !m_isWireframe; }

    private:
        float CalculateHeight(
            const uint8_t r,
            const uint8_t g,
            const uint8_t b,
            const float maxPixelColor = 255.0f * 255.0f * 255.0f);

        void Initialize();

        void GenerateHeightMapData();
        void GenerateMeshData();
        XMVECTOR GenerateVertexNormal(uint32_t index);


        bool  m_isWireframe{ false };
        float m_minHeight;  // the lowest possible point in the terrain
        float m_maxHeight;  // the highest possible point in the terrain

        std::shared_ptr<Image> m_heightMapImage;

        std::vector<Vertex>            m_heightMapVertices;
        std::vector<uint32_t>          m_heightMapIndices;
        std::vector<FaceNormalIndexed> m_heightMapFaces;

        std::vector<Vertex>   m_meshVertices;
        std::vector<uint32_t> m_meshIndices;

        ComPtr<ID3D11ShaderResourceView> m_baseTexture;
        ComPtr<ID3D11SamplerState>       m_sampler;
    };
}