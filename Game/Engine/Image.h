#pragma once
#include <string>
#include <vector>

namespace KerpEngine
{
		class Image
		{
		public:
				static const int32_t DEFAULT_WIDTH = 512;
				static const int32_t DEFAULT_HEIGHT = 512;

		public:
				struct Pixel {
						uint8_t red;
						uint8_t green;
						uint8_t blue;
						uint8_t alpha;
						uint32_t row;
						uint32_t column;
				};

		public:
				Image(
						const std::wstring filename,
						const int32_t width = DEFAULT_WIDTH,
						const int32_t height = DEFAULT_HEIGHT) : m_filename(filename), m_width(width), m_height(height) {};
				virtual ~Image() {};
				const std::vector<uint8_t>* Data() { return &m_data; }
				const std::vector<Pixel>* Pixels() { return &m_pixelData; }
				const int32_t Height() { return m_height; }
				const int32_t Width() { return m_width; }

		protected:
				virtual void LoadPixelData() = 0;
				virtual const bool Read(const std::wstring filename) = 0;

				std::wstring m_filename;
				int32_t m_width;
				int32_t m_height;
				std::vector<uint8_t> m_data;
				std::vector<Pixel> m_pixelData;
		};
}

