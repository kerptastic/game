#pragma once
#include "Image.h"
#include <string>
#include <vector>

namespace KerpEngine
{
		class RawImage : public Image
		{
		public:
				static const std::wstring DEFAULT_FILENAME;
				static const uint32_t DATA_CHUNK_OFFSET;
				static const uint32_t RED_OFFSET;
				static const uint32_t BLUE_OFFSET;
				static const uint32_t GREEN_OFFSET;
				static const uint32_t ALPHA_OFFSET;
		public:
				RawImage(
						const std::wstring filename,
						int32_t width = 512,
						int32_t height = 512);
				~RawImage();

		protected:
				void LoadPixelData();
				const bool Read(const std::wstring rawImageFile);

		};
}