#include "pch.h"
#include "RawImage.h"
#include <fstream>

using namespace KerpEngine;

const std::wstring RawImage::DEFAULT_FILENAME = L"Content\\default.raw";
const uint32_t RawImage::DATA_CHUNK_OFFSET = 4;
const uint32_t RawImage::RED_OFFSET = 0;
const uint32_t RawImage::BLUE_OFFSET = 1;
const uint32_t RawImage::GREEN_OFFSET = 2;
const uint32_t RawImage::ALPHA_OFFSET = 3;

// Public constructor
RawImage::RawImage(const std::wstring filename, int32_t width, int32_t height) : Image(filename, width, height) {
    bool success = true;

    if (!this->Read(filename))
    {
        // TODO: initial bitmap load failed, using default

        if (!this->Read(DEFAULT_FILENAME))
        {
            // TODO: debugging message trying to load default
            // TODO: big problem here if the default image didnt load
            success = false;
        }
    }

    if (success) {
        LoadPixelData();
    }
}

// Public destructor
RawImage::~RawImage() {
}

// Reads the RAW file
const bool RawImage::Read(const std::wstring filename) {
    m_filename = filename;

    std::ifstream rawFS(m_filename.c_str(), std::ios_base::binary);

    if (!rawFS) {
        return false;
    }

    // set the stream to the end of the file, to them use tellg to get the current
    // location - which will be the length. then move back to the beginning of the
    // file, resize the data vector and then read it all in.
    rawFS.seekg(0, rawFS.end);
    uint32_t length = (uint32_t)rawFS.tellg();

    if (length != (m_width * m_height * DATA_CHUNK_OFFSET)) {
        return false;
    }

    rawFS.seekg(0, rawFS.beg);

    m_data.resize(length);

    rawFS.read((char*)& m_data[0], length);

    return true;
}

//
void RawImage::LoadPixelData() {
    uint8_t dataR, dataG, dataB, dataA;
    uint32_t row, col;

    for (uint32_t p = 0; p < m_data.size(); p += DATA_CHUNK_OFFSET) {
        dataR = m_data.at(p + RED_OFFSET);
        dataG = m_data.at(p + BLUE_OFFSET);
        dataB = m_data.at(p + GREEN_OFFSET);
        dataA = m_data.at(p + ALPHA_OFFSET);

        row = (p / DATA_CHUNK_OFFSET) / m_width;
        col = (p / DATA_CHUNK_OFFSET) - (row * m_width);

        m_pixelData.push_back({ dataR, dataG, dataB, dataA, row, col });
    }
}