#pragma once

#include <Engine\GameObject.h>
#include "SpriteBatch.h"

using namespace Microsoft::WRL;

namespace KerpEngine
{
	class Sprite : public GameObject
	{
	public:
		Sprite(
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			ComPtr<ID3D11ShaderResourceView>& texture);
		Sprite(
			const std::shared_ptr<DX::DeviceResources>& deviceResources,
			ComPtr<ID3D11ShaderResourceView>& texture,
			const XMVECTOR position);
		virtual ~Sprite();

		void CreateDeviceDependentResources();
		void CreateWindowSizeDependentResources();
		void ReleaseDeviceDependentResources();

		void Initialize();
		virtual void Update(DX::StepTimer const& timer);
		virtual void Render(const XMFLOAT4X4 view, const XMFLOAT4X4 projection);

	protected:
		ComPtr<ID3D11InputLayout>	m_inputLayout;
		ComPtr<ID3D11Buffer>		m_vertexBuffer;
		ComPtr<ID3D11Buffer>		m_indexBuffer;
		ComPtr<ID3D11VertexShader>	m_vertexShader;
		ComPtr<ID3D11PixelShader>	m_pixelShader;
		ComPtr<ID3D11Buffer>		m_constantBuffer;

		ComPtr<ID3D11ShaderResourceView> m_texture;
		std::unique_ptr<DirectX::SpriteBatch> m_spriteBatch;

	private:

	};
}
