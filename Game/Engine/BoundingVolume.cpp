#include "pch.h"
#include "BoundingVolume.h"

using namespace KerpEngine;

namespace KerpEngine {
    // Creates a new BoundingVolume, setting device resources, position, and lookAt vectors.
    BoundingVolume::BoundingVolume(
        const std::shared_ptr<DX::DeviceResources>& deviceResources,
        const XMFLOAT3 position) :
        GameObject(deviceResources, position)
    {
    }

    // Public destructor.
    BoundingVolume::~BoundingVolume()
    {
    }
}