#include "pch.h"
#include "OABB.h"
#include "..\Common\DirectXHelper.h"
#include "EngineUtils.h"

#include <cfloat>
#include <cmath>

using namespace DirectX;

namespace KerpEngine
{
    // Creates a 2D OABB, which determines it's top right and bottom left boundaries
    // via the position, width, and height.
    OABB::OABB(const std::shared_ptr<DX::DeviceResources>& deviceResources,
        const XMFLOAT3 position, const float width, const float height, const float depth) : OABB(
            deviceResources,
            position,
            { width / 2.0f, height / 2.0f, depth / 2.0f },
            { -width / 2.0f, -height / 2.0f, -depth / 2.0f })
    {
    }

    // Creates a 3D OABB, determining its position based on the given top right and
    // bottom left locations.
    OABB::OABB(const std::shared_ptr<DX::DeviceResources>& deviceResources,
        const XMFLOAT3 frontTopRight, const XMFLOAT3 backBottomLeft) : OABB(
            deviceResources,
            {
                (frontTopRight.x + backBottomLeft.x) / 2.0f,
                (frontTopRight.y + backBottomLeft.y) / 2.0f,
                (frontTopRight.z + backBottomLeft.z) / 2.0f
            },
            frontTopRight,
            backBottomLeft
        )
    {
    }

    // Creates a 3D OABB in the given position, with the given top right and bottom left
    // locations.
    OABB::OABB(const std::shared_ptr<DX::DeviceResources>& deviceResources,
        const XMFLOAT3 position, const XMFLOAT3 frontTopRight, const XMFLOAT3 backBottomLeft) :
        BoundingVolume(deviceResources, position),
        m_frontTopRight(frontTopRight),
        m_backBottomLeft(backBottomLeft)
    {
        Initialize();
    }

    // Public Destructor.
    OABB::~OABB()
    {
    }

    // Initializes the OABB.
    void OABB::Initialize()
    {
        m_width = abs(m_frontTopRight.x - m_backBottomLeft.x);
        m_height = abs(m_frontTopRight.y - m_backBottomLeft.y);
        m_depth = abs(m_frontTopRight.z - m_backBottomLeft.z);

        // set the other corners using the frontTopRight and backBottomLeft values
        m_frontTopLeft = { m_backBottomLeft.x, m_frontTopRight.y, m_frontTopRight.z };
        m_frontBottomRight = { m_frontTopRight.x, m_backBottomLeft.y, m_frontTopRight.z };
        m_frontBottomLeft = { m_backBottomLeft.x, m_backBottomLeft.y, m_frontTopRight.z };

        m_backTopRight = { m_frontTopRight.x, m_frontTopRight.y, m_backBottomLeft.z };
        m_backTopLeft = { m_backBottomLeft.x, m_frontTopRight.y, m_backBottomLeft.z };
        m_backBottomRight = { m_frontTopRight.x, m_backBottomLeft.y, m_backBottomLeft.z };

        CreateDeviceDependentResources();
        CreateWindowSizeDependentResources();
    }

    // Updates the world vertices into translated vertices to use the corners for collision detection.
    void OABB::UpdateCollisionPoints()
    {
        XMMATRIX model =
            XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z) *
            XMMatrixRotationRollPitchYaw(m_rotation.x, m_rotation.y, m_rotation.z) *
            XMMatrixTranslation(m_position.x, m_position.y, m_position.z);

        XMStoreFloat3(&m_collisionFrontTopRight, XMVector3Transform(XMLoadFloat3(&m_frontTopRight), model));
        XMStoreFloat3(&m_collisionFrontTopLeft, XMVector3Transform(XMLoadFloat3(&m_frontTopLeft), model));
        XMStoreFloat3(&m_collisionFrontBottomRight, XMVector3Transform(XMLoadFloat3(&m_frontBottomRight), model));
        XMStoreFloat3(&m_collisionFrontBottomLeft, XMVector3Transform(XMLoadFloat3(&m_frontBottomLeft), model));

        XMStoreFloat3(&m_collisionBackTopRight, XMVector3Transform(XMLoadFloat3(&m_backTopRight), model));
        XMStoreFloat3(&m_collisionBackTopLeft, XMVector3Transform(XMLoadFloat3(&m_backTopLeft), model));
        XMStoreFloat3(&m_collisionBackBottomRight, XMVector3Transform(XMLoadFloat3(&m_backBottomRight), model));
        XMStoreFloat3(&m_collisionBackBottomLeft, XMVector3Transform(XMLoadFloat3(&m_backBottomLeft), model));

        m_collisionCorners.clear();
        m_collisionCorners.push_back(m_collisionFrontTopRight);
        m_collisionCorners.push_back(m_collisionFrontTopLeft);
        m_collisionCorners.push_back(m_collisionFrontBottomLeft);
        m_collisionCorners.push_back(m_collisionFrontBottomRight);
        m_collisionCorners.push_back(m_collisionBackTopRight);
        m_collisionCorners.push_back(m_collisionBackTopLeft);
        m_collisionCorners.push_back(m_collisionBackBottomRight);
        m_collisionCorners.push_back(m_collisionBackBottomLeft);
    }

    // Returns a normal vector for the requested Face of the OABB.
    const XMVECTOR OABB::GetCollisionFaceNormal(const Face face)
    {
        XMVECTOR normal;

        switch (face)
        {
        case FRONT:
            normal = EngineUtils::GetSurfaceNormal(
                XMLoadFloat3(&m_collisionFrontTopRight),
                XMLoadFloat3(&m_collisionFrontBottomRight),
                XMLoadFloat3(&m_collisionFrontBottomLeft));
            break;
        case BACK:
            normal = EngineUtils::GetSurfaceNormal(
                XMLoadFloat3(&m_collisionBackTopLeft),
                XMLoadFloat3(&m_collisionBackBottomLeft),
                XMLoadFloat3(&m_collisionBackBottomRight));
            break;
        case TOP:
            normal = EngineUtils::GetSurfaceNormal(
                XMLoadFloat3(&m_collisionFrontTopRight),
                XMLoadFloat3(&m_collisionBackTopLeft),
                XMLoadFloat3(&m_collisionBackTopRight));
            break;
        case BOTTOM:
            normal = EngineUtils::GetSurfaceNormal(
                XMLoadFloat3(&m_collisionBackBottomRight),
                XMLoadFloat3(&m_collisionBackBottomLeft),
                XMLoadFloat3(&m_collisionFrontBottomRight));
            break;
        case LEFT:
            normal = EngineUtils::GetSurfaceNormal(
                XMLoadFloat3(&m_collisionFrontTopLeft),
                XMLoadFloat3(&m_collisionBackBottomLeft),
                XMLoadFloat3(&m_collisionBackTopLeft));
            break;
        case RIGHT:
            normal = EngineUtils::GetSurfaceNormal(
                XMLoadFloat3(&m_collisionFrontTopRight),
                XMLoadFloat3(&m_collisionBackTopRight),
                XMLoadFloat3(&m_collisionFrontBottomRight));
            break;
        default:
            //TODO: throw exception 
            break;
        }

        return normal;
    }

    // Detects an intersection between this OABB and another Bounding Volume.
    IntersectionType OABB::Intersects(const std::shared_ptr<BoundingVolume>& other)
    {
        // TODO: instance of check, intersecting a bounding sphere with a bounding box
        // requires different calculations

        if (std::shared_ptr<OABB> theOther = std::dynamic_pointer_cast<OABB>(other))
        {
            // Explanation: this is an implementation of the seperated axis theorem. How it works
            // is for each volume, we work one at a time - projecting each face of the volume onto
            // an axis perpendicular from the surface normal. In order to project, we take every corner
            // of the volume to find the min and max points we take the dot product of each corner
            // (vertex) with the normal, and keep track of the mix max points. We determine that for
            // both of the volumes, which give us a line on the perpendicular axis for both objects.
            // if those lines do NOT operlap, then there is an axis that seperates the two objects
            // and means they do NOT collide. We do this for ALL the normals for both volumes in search
            // of a Seperated Axis. More info see: http://www.dyn4j.org/2010/01/sat/
            bool foundSeperatedAxis = false;
            float myMin, myMax, theirMin, theirMax;
            std::vector<XMFLOAT3> myCorners, theirCorners;

            // TODO: optimization - need to be more directed on normals and getting only the corners
            // that are specific to the normal being tested, for example, when using the normal for the
            // top face, the top corners are necessary, but the bottom corners are not since they are
            // identical points that will produce the same max/min
            for each (Face face in KerpEngine::Enums::FaceValues3D)
            {
                XMVECTOR n = GetCollisionFaceNormal(face);

                myMin = theirMin = FLT_MAX;
                myMax = theirMax = FLT_MIN;

                myCorners = GetCollisionFaceCorners(face);
                theirCorners = theOther->GetCollisionFaceCorners(face);

                for each (XMFLOAT3 c in myCorners) {
                    float dot = XMVectorGetX(XMVector3Dot(n, XMLoadFloat3(&c)));

                    myMin = __min(dot, myMin);
                    myMax = __max(dot, myMax);
                }

                for each (XMFLOAT3 c in theirCorners) {
                    float dot = XMVectorGetX(XMVector3Dot(n, XMLoadFloat3(&c)));

                    theirMin = __min(dot, theirMin);
                    theirMax = __max(dot, theirMax);
                }

                // fully left, then fully right
                if (myMax < theirMin || myMin > theirMax)
                {
                    return NONE;
                }
            }

            for each (Face face in KerpEngine::Enums::FaceValues3D)
            {
                XMVECTOR n = theOther->GetCollisionFaceNormal(face);

                myMin = theirMin = FLT_MAX;
                myMax = theirMax = FLT_MIN;

                myCorners = GetCollisionFaceCorners(face);
                theirCorners = theOther->GetCollisionFaceCorners(face);

                for each (XMFLOAT3 c in myCorners) {
                    float dot = XMVectorGetX(XMVector3Dot(n, XMLoadFloat3(&c)));

                    myMin = __min(dot, myMin);
                    myMax = __max(dot, myMax);
                }

                for each (XMFLOAT3 c in theirCorners) {
                    float dot = XMVectorGetX(XMVector3Dot(n, XMLoadFloat3(&c)));

                    theirMin = __min(dot, theirMin);
                    theirMax = __max(dot, theirMax);
                }

                // fully left, then fully right
                if (myMax < theirMin || myMin > theirMax)
                {
                    return NONE;
                }
            }
        }

        return INTERSECTS;
    }

    // Whether or not this OABB contains a vector v.
    IntersectionType OABB::Contains(const XMFLOAT3 v)
    {
        return NONE;
    }

    // Updates the position and orientation of this OABB in the world.
    void OABB::Update(DX::StepTimer const& timer)
    {
        UpdateCollisionPoints();
    }

    // Renders the wireframe of the OABB.
    void OABB::Render(const XMMATRIX view, const XMMATRIX projection)
    {
        // Loading is asynchronous. Only draw geometry after it's loaded.
        if (!m_loadingComplete)
        {
            return;
        }

        XMMATRIX model =
            XMMatrixTranspose(XMMatrixTranslation(m_position.x, m_position.y, m_position.z)) *
            XMMatrixTranspose(XMMatrixRotationRollPitchYaw(m_rotation.x, m_rotation.y, m_rotation.z)) *
            XMMatrixTranspose(XMMatrixScaling(m_scale.x, m_scale.y, m_scale.z));

        // Prepare to pass the updated model matrix to the shader
        XMStoreFloat4x4(&m_constantBufferData.model, model);
        XMStoreFloat4x4(&m_constantBufferData.view, view);
        XMStoreFloat4x4(&m_constantBufferData.projection, projection);

        auto context = m_deviceResources->GetD3DDeviceContext();

        // Prepare the constant buffer to send it to the graphics device.
        context->UpdateSubresource1(m_constantBuffer.Get(), 0, NULL, &m_constantBufferData, 0, 0, 0);

        // Each vertex is one instance of the Vertex struct.
        UINT stride = sizeof(Vertex);
        UINT offset = 0;
        context->IASetVertexBuffers(0, 1, m_vertexBuffer.GetAddressOf(), &stride, &offset);

        context->IASetIndexBuffer(m_indexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0);
        context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
        context->IASetInputLayout(m_inputLayout.Get());

        // Attach our vertex shader.
        context->VSSetShader(m_vertexShader.Get(), nullptr, 0);

        // Send the constant buffer to the graphics device.
        context->VSSetConstantBuffers1(0, 1, m_constantBuffer.GetAddressOf(), nullptr, nullptr);

        // Attach our pixel shader.
        context->PSSetShader(m_pixelShader.Get(), nullptr, 0);

        // Draw the objects.
        context->DrawIndexed(m_indexCount, 0, 0);
    }

    // Creates resources that are initialized when the Direct3D device is initialized.
    void OABB::CreateDeviceDependentResources()
    {
        // Load shaders asynchronously.
        auto loadVSTask = DX::ReadDataAsync(L"SampleVertexShader.cso");
        auto loadPSTask = DX::ReadDataAsync(L"SamplePixelShader.cso");

        // After the vertex shader file is loaded, create the shader and input layout.
        auto createVSTask = loadVSTask.then([this](const std::vector<byte>& fileData) {
            DX::ThrowIfFailed(
                m_deviceResources->GetD3DDevice()->CreateVertexShader(&fileData[0], fileData.size(), nullptr, &m_vertexShader)
            );

            static const D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
            {
                { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
                { "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
            };

            DX::ThrowIfFailed(
                m_deviceResources->GetD3DDevice()->CreateInputLayout(vertexDesc, ARRAYSIZE(vertexDesc), &fileData[0], fileData.size(), &m_inputLayout)
            );
        });

        // After the pixel shader file is loaded, create the shader and constant buffer.
        auto createPSTask = loadPSTask.then([this](const std::vector<byte>& fileData) {
            DX::ThrowIfFailed(
                m_deviceResources->GetD3DDevice()->CreatePixelShader(&fileData[0], fileData.size(), nullptr, &m_pixelShader)
            );

            CD3D11_BUFFER_DESC constantBufferDesc(sizeof(ModelViewProjectionConstantBuffer), D3D11_BIND_CONSTANT_BUFFER);
            DX::ThrowIfFailed(
                m_deviceResources->GetD3DDevice()->CreateBuffer(&constantBufferDesc, nullptr, &m_constantBuffer)
            );
        });

        // Once both shaders are loaded, create the mesh.
        auto createCubeTask = (createPSTask && createVSTask).then([this]() {
            // Load mesh vertices. Each vertex has a position and a color.
            static const Vertex cubeVertices[] =
            {
                // left, bottom, front
                {
                    {m_backBottomLeft.x, m_backBottomLeft.y, m_frontTopRight.z}, {1.0f, 0.0f, 0.0f}
                },
                // left, top, front
                {
                    {m_backBottomLeft.x, m_frontTopRight.y, m_frontTopRight.z}, {1.0f, 0.0f, 0.0f}
                },
                // right, top, front
                {
                    {m_frontTopRight.x, m_frontTopRight.y, m_frontTopRight.z}, {1.0f, 0.0f, 0.0f}
                },
                // right, bottom, front
                {
                    { m_frontTopRight.x, m_backBottomLeft.y, m_frontTopRight.z}, {1.0f, 0.0f, 0.0f}
                },

                // left, bottom, back
                {
                    {m_backBottomLeft.x, m_backBottomLeft.y, m_backBottomLeft.z}, {1.0f, 0.0f, 0.0f}
                },
                // left, top, back
                {
                    {m_backBottomLeft.x, m_frontTopRight.y, m_backBottomLeft.z}, {1.0f, 0.0f, 0.0f}
                },
                // right, top, back
                {
                    {m_frontTopRight.x, m_frontTopRight.y, m_backBottomLeft.z}, {1.0f, 0.0f, 0.0f}
                },
                // right, bottom, back
                {
                    {m_frontTopRight.x, m_backBottomLeft.y, m_backBottomLeft.z}, {1.0f, 0.0f, 0.0f}
                },
            };

            D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
            vertexBufferData.pSysMem = cubeVertices;
            vertexBufferData.SysMemPitch = 0;
            vertexBufferData.SysMemSlicePitch = 0;
            CD3D11_BUFFER_DESC vertexBufferDesc(sizeof(cubeVertices), D3D11_BIND_VERTEX_BUFFER);
            DX::ThrowIfFailed(
                m_deviceResources->GetD3DDevice()->CreateBuffer(
                    &vertexBufferDesc,
                    &vertexBufferData,
                    &m_vertexBuffer
                )
            );

            // Load mesh indices. Each trio of indices represents
            // a triangle to be rendered on the screen.
            // For example: 0,2,1 means that the vertices with indexes
            // 0, 2 and 1 from the vertex buffer compose the 
            // first triangle of this mesh.
            static const unsigned short cubeIndices[] =
            {
                // front
                0, 1, 1, 2, 2, 3, 3, 0,
                // back
                4, 5, 5, 6, 6, 7, 7, 4,
                // left
                1, 5, 0, 4,
                // right
                2, 6, 3, 7
            };

            m_indexCount = ARRAYSIZE(cubeIndices);

            D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
            indexBufferData.pSysMem = cubeIndices;
            indexBufferData.SysMemPitch = 0;
            indexBufferData.SysMemSlicePitch = 0;
            CD3D11_BUFFER_DESC indexBufferDesc(sizeof(cubeIndices), D3D11_BIND_INDEX_BUFFER);
            DX::ThrowIfFailed(
                m_deviceResources->GetD3DDevice()->CreateBuffer(
                    &indexBufferDesc,
                    &indexBufferData,
                    &m_indexBuffer
                )
            );
        });

        // Once the cube is loaded, the object is ready to be rendered.
        createCubeTask.then([this]() {
            m_loadingComplete = true;
        });
    }

    // Creates resources that need to be updated when the window size is updated.
    void OABB::CreateWindowSizeDependentResources()
    {
    }
}