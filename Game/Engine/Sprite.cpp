#include "pch.h"
//#include "Sprite.h"
//#include "..\Common\DirectXHelper.h"
//
//using namespace KerpEngine;
//
////
//Sprite::Sprite(const std::shared_ptr<DX::DeviceResources>& deviceResources,
//	ComPtr<ID3D11ShaderResourceView>& texture)
//	: Sprite(deviceResources, texture, { 0.0f, 0.0f, 0.0f })
//{
//}
//
////
//Sprite::Sprite(const std::shared_ptr<DX::DeviceResources>& deviceResources,
//	ComPtr<ID3D11ShaderResourceView>& texture,
//	const XMVECTOR position) : 
//	GameObject(deviceResources, position, { 0.0f, 0.0f, 1.0f }),
//	m_texture(texture)
//{
//	Initialize();
//}
//
////
//Sprite::~Sprite()
//{
//}
//
////
//void Sprite::Initialize()
//{
//	m_spriteBatch = std::make_unique<SpriteBatch>(m_deviceResources->GetD3DDeviceContext());
//}
//\
//// Updates the Object's lookAt direction based on its position and rotation.
//void Sprite::Update(DX::StepTimer const& timer)
//{
//	XMVECTOR transReference = XMVector3Transform(
//		m_referenceLookAt,
//		XMMatrixRotationQuaternion(m_orientation));
//
//	XMFLOAT3 transReferenceF;
//	XMStoreFloat3(&transReferenceF, transReference);
//
//	XMFLOAT3 pos;
//	XMStoreFloat3(&pos, m_position);
//
//	m_lookAt = {
//		pos.x + transReferenceF.x,
//		pos.y + transReferenceF.y,
//		pos.z + transReferenceF.z
//	};
//}
//
//// Renders this object to the screen, using the given view and projection matrices.
//void Sprite::Render(const XMFLOAT4X4 view, const XMFLOAT4X4 projection)
//{
//	// Loading is asynchronous. Only draw geometry after it's loaded.
//	/*if (!m_loadingComplete)
//	{
//		return;
//	}*/
//
//	//ComPtr<ID3D11Resource> resource;
//	//ComPtr<ID3D11Texture2D> tex;
//	//DX::ThrowIfFailed(m_texture.As(&tex));
//
//	//CD3D11_TEXTURE2D_DESC texDesc;
//	//m_texture->GetDesc(&texDesc);
//
//	/*float w = float(texDesc.ViewDimension / 2);
//	float h = float(texDesc.Height / 2);
//	*/
//
//	m_spriteBatch->Begin();
//
//	m_spriteBatch->Draw(m_texture.Get(), RECT { 200, 200 }, nullptr, Colors::White,
//		0.f, XMFLOAT2 { 512.0f, 512.0f });
//
//	m_spriteBatch->End();
//}
//
////
//void Sprite::CreateDeviceDependentResources()
//{
//}
//
////
//void Sprite::CreateWindowSizeDependentResources()
//{
//}
//
//// Release all device resources.
//void Sprite::ReleaseDeviceDependentResources()
//{
//	m_loadingComplete = false;
//	m_vertexShader.Reset();
//	m_inputLayout.Reset();
//	m_pixelShader.Reset();
//	m_constantBuffer.Reset();
//	m_vertexBuffer.Reset();
//	m_indexBuffer.Reset();
//}
