#pragma once

#include "..\Common\DeviceResources.h"
#include "..\Common\CustomStructures.h"
#include "..\Common\StepTimer.h"

using namespace DirectX;
using namespace Microsoft::WRL;

namespace KerpEngine
{
    // forward declaration to avoid circular includes with BoundingVolume.h
    class BoundingVolume;

    // Encapsulation of a game object, tracking position and orientation, and bounding
    // volume - while also responsible for updating and rendering.
    class GameObject
    {
    public:
        GameObject(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position);
        virtual ~GameObject() {};

        const XMFLOAT3 GetPosition() { return m_position; }
        void SetPosition(const XMFLOAT3 position) { m_position = position; }

        const XMFLOAT3 GetRotation() { return m_rotation; }
        void SetRotation(const XMFLOAT3 rotation) { m_rotation = rotation; }

        const XMFLOAT3 GetScale() { return m_scale; }
        void SetScale(const XMFLOAT3 scale) { m_scale = scale; }

        const std::shared_ptr<BoundingVolume> GetBoundingVolume() { return m_boundingVolume; }

        virtual void Move(const XMFLOAT3 amount);
        virtual void Rotate(const XMFLOAT3 pyr);
        virtual void Scale(const XMFLOAT3 scale);

        virtual void Update(DX::StepTimer const& timer);
        virtual void Render(const XMMATRIX view, const XMMATRIX projection) = 0;

        virtual void CreateDeviceDependentResources() = 0;
        virtual void CreateWindowSizeDependentResources() = 0;
        virtual void ReleaseDeviceDependentResources();


        // see: https://stackoverflow.com/questions/20104815/warning-c4316-object-allocated-on-the-heap-may-not-be-aligned-16
        void* operator new(size_t i)
        {
            return _mm_malloc(i, 16);
        }

        // see: https://stackoverflow.com/questions/20104815/warning-c4316-object-allocated-on-the-heap-may-not-be-aligned-16
        void operator delete(void* p)
        {
            _mm_free(p);
        }

    protected:
        void Initialize();

        bool m_loadingComplete;

        std::shared_ptr<DX::DeviceResources> m_deviceResources;
        ModelViewProjectionConstantBuffer    m_constantBufferData;

        XMFLOAT3 m_position;
        XMFLOAT3 m_rotation;
        XMFLOAT3 m_scale;
        XMFLOAT3 m_up;
        XMFLOAT3 m_lookAt;
        XMFLOAT3 m_referenceLookAt;
        XMFLOAT4 m_orientation;

        std::shared_ptr<BoundingVolume> m_boundingVolume;

        uint32	m_indexCount;

        ComPtr<ID3D11InputLayout>	m_inputLayout;
        ComPtr<ID3D11Buffer>		m_vertexBuffer;
        ComPtr<ID3D11Buffer>		m_indexBuffer;
        ComPtr<ID3D11VertexShader>	m_vertexShader;
        ComPtr<ID3D11PixelShader>	m_pixelShader;
        ComPtr<ID3D11Buffer>		m_constantBuffer;

        ComPtr<ID3D11RasterizerState> m_wireframeRasterizerState;
        ComPtr<ID3D11RasterizerState> m_solidRasterizerState;

        D3D11_RASTERIZER_DESC m_wireframeRasterizerDesc;
        D3D11_RASTERIZER_DESC m_solidRasterizerDesc;

    private:

    };
}

