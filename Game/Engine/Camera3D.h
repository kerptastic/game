#pragma once

#include <string>
#include "GameObject.h"

using namespace DirectX;

namespace KerpEngine {
    class Camera3D : public GameObject {
    public:
        static const float DEFAULT_ASPECT_RATIO;
        static const float DEFAULT_FAR_PLANE;
        static const float DEFAULT_FOV_ANGLE_Y;
        static const float DEFAULT_NEAR_PLANE;
        static const float MAX_PITCH;

    public:
        Camera3D(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position,
            float farPlane = DEFAULT_FAR_PLANE,
            float nearPlane = DEFAULT_NEAR_PLANE,
            float aspectRatio = DEFAULT_ASPECT_RATIO,
            float fovAngleY = DEFAULT_FOV_ANGLE_Y);
        ~Camera3D();

        void Initialize();
        void InitViewMatrix();

        void CreateDeviceDependentResources() {};
        void CreateWindowSizeDependentResources() {};

        void Update(DX::StepTimer const& timer);
        void Render(const XMMATRIX view, const XMMATRIX projection) {};

        const XMMATRIX GetProjectionMatrix() { return XMLoadFloat4x4(&m_projection); };
        const XMMATRIX GetViewMatrix() { return XMLoadFloat4x4(&m_view); };

        const std::wstring GetDiagText();

    protected:
        float	m_aspectRatio;
        float	m_nearPlane;
        float	m_farPlane;
        float	m_fovAngleY;
        float	m_zoom;

        XMFLOAT4X4 m_view;
        XMFLOAT4X4 m_perspective;
        XMFLOAT4X4 m_projection;
    };
}

