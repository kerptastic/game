Texture2D shaderTexture;
SamplerState sampleType;

// A constant buffer that stores the three basic column-major matrices for composing geometry.
cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
    matrix model;
    matrix view;
    matrix projection;
    float4 lightDirection;
    float4 lightColor;
    float4 outputColor;
};

// Per-pixel color data passed through the pixel shader.
struct PixelShaderInput
{
    float4 pos : SV_POSITION;
    float3 color : COLOR0;
    float3 normal : NORMAL;
    float2 tex0 : TEXCOORD0;
};


// A pass-through function for the (interpolated) color data.
float4 main(PixelShaderInput input) : SV_TARGET
{
    float4 textureColor = shaderTexture.Sample(sampleType, input.tex0);

    //float3 lightDir = -lightDirection;

    float3 dir;

    float lightIntensity = saturate(dot((float3)(lightDirection * -1.0f), input.normal));
    float4 color = 1;

    color = saturate(textureColor * lightIntensity);
    color.a = 1;
    return color;
}
