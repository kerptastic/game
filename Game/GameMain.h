﻿#pragma once

#include "Common\StepTimer.h"
#include "Common\DeviceResources.h"
#include "Content\SampleFpsTextRenderer.h"

#include "Engine\GameObject.h"
#include "Engine\Sprite.h"
#include "Engine\Camera3D.h"
#include "Engine\TextureManager.h"
#include "Engine\Image.h"
#include "Engine\Terrain3D.h"
#include "Testing\TestObject2D.h"
#include "Testing\TestObject3D.h"

#include "Keyboard.h"
#include "Mouse.h"
#include "GamePad.h"

using namespace KerpEngine;

// Renders Direct2D and 3D content on the screen.
namespace Game
{
	class GameMain : public DX::IDeviceNotify
	{
	public:
		GameMain(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		~GameMain();

		void CreateWindowSizeDependentResources();
		void CreateInputDevices();
		void Update();
		bool Render();

		// IDeviceNotify
		virtual void OnDeviceLost();
		virtual void OnDeviceRestored();

	private:
		// Cached pointer to device resources.
		std::shared_ptr<DX::DeviceResources> m_deviceResources;

		// TODO: Replace with your own content renderers.
		std::unique_ptr<TestObject3D> m_gameObject1;
		std::unique_ptr<TestObject3D> m_gameObject2;
		std::unique_ptr<TestObject2D> m_gameObject2D1;
		std::unique_ptr<TestObject2D> m_gameObject2D2;
		std::shared_ptr<Image>        m_heightMapImage;
		std::unique_ptr<Terrain3D>    m_terrain;

		std::unique_ptr<Sprite> m_sprite;
		std::unique_ptr<SampleFpsTextRenderer> m_textRenderer;

		std::shared_ptr<TextureManager> m_textureManager;

		std::unique_ptr<Camera3D> m_camera;
		std::unique_ptr<Keyboard> m_keyboard;
		std::unique_ptr<Mouse> m_mouse;
		std::unique_ptr<GamePad> m_gamepad;

		// Rendering loop timer.
		DX::StepTimer m_timer;
	};
}