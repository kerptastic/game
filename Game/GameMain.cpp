﻿#include "pch.h"
#include "GameMain.h"
#include "Common\DirectXHelper.h"
#include "Engine\BoundingVolume.h"
#include "Engine\Camera3DFirstPerson.h"
#include "Engine\RawImage.h"
#include "Engine\Bitmap.h"

using namespace Game;
using namespace Windows::Devices::Input;
using namespace Windows::Graphics::Display;
using namespace Windows::Foundation;
using namespace Windows::System::Threading;
using namespace Concurrency;

// Loads and initializes application assets when the application is loaded.
GameMain::GameMain(const std::shared_ptr<DX::DeviceResources>& deviceResources) :
    m_deviceResources(deviceResources)
{
    // Register to be notified if the Device is lost or recreated
    m_deviceResources->RegisterDeviceNotify(this);

    // texture creation and loading
    m_textureManager = TextureManager::GetInstance(deviceResources);
    //m_textureManager->Load2DTexture(L"Content\\grass.jpg");
    m_textureManager->Load2DTexture(L"Content\\default.bmp");
    ComPtr<ID3D11ShaderResourceView>& texture = m_textureManager->GetTexture2D(L"grass");

    // camera test creation
    m_camera = std::make_unique<Camera3DFirstPerson>(
        m_deviceResources, XMFLOAT3{ 40.0f, 0.0f, 40.0f }, 4000.0f);
    //m_deviceResources, { 40.0f, 400.0f, 15.0f }, 4000.0f);

// 3d game object test creation
    m_gameObject1 = std::make_unique<TestObject3D>(
        m_deviceResources, XMFLOAT3{ 0.0f, 0.0f, 10.0f });

    m_gameObject2 = std::make_unique<TestObject3D>(
        m_deviceResources, XMFLOAT3{ -3.0f, 0.0f, 10.0f });

    // 2d
    m_gameObject2D1 = std::make_unique<TestObject2D>(
        m_deviceResources, XMFLOAT3{ 0.0f, 2.5f, 10.0f }, texture);

    m_gameObject2D2 = std::make_unique<TestObject2D>(
        m_deviceResources, XMFLOAT3{ -3.0f, 2.5f, 10.0f }, texture);

    m_textRenderer = std::make_unique<SampleFpsTextRenderer>(m_deviceResources);

    // test loading heightmaps
    //m_heightMapImage = std::make_shared<RawImage>(L"Content\\terrain.raw");
    m_heightMapImage = std::make_shared<Bitmap>(L"Content\\terrain3.bmp");
    //m_heightMapImage = std::make_shared<Bitmap>(L"Content\\10x10.bmp");


    m_terrain = std::make_unique<Terrain3D>(
        m_deviceResources,
        XMFLOAT3{ 0.0f, 0.0f, 00.0f },
        texture,
        m_heightMapImage);

    //m_terrain->SetScale({ 20.0f, 20.0f, 20.0f });
    //m_terrain->Rotate({ 0.0f, 90.0f, 0.0f });

    // TODO: Change the timer settings if you want something other than the default variable timestep mode.
    // e.g. for 60 FPS fixed timestep update logic, call:
    //m_timer.SetFixedTimeStep(true);
    //m_timer.SetTargetElapsedSeconds(1.0 / 60);

    CreateInputDevices();
}

// Public Destructor.
GameMain::~GameMain()
{
    // Deregister device notification
    m_deviceResources->RegisterDeviceNotify(nullptr);
}

// Updates application state when the window size changes (e.g. device orientation change)
void GameMain::CreateWindowSizeDependentResources()
{
    // TODO: Replace this with the size-dependent initialization of your app's content.

    // TODO: (jk) move objects into here when you care about resizing the window

    //m_sceneRenderer->CreateWindowSizeDependentResources();
}

// Creates input devices.
void GameMain::CreateInputDevices()
{
    m_keyboard = std::make_unique<Keyboard>();
    m_keyboard->SetWindow(m_deviceResources->GetWindow());

    m_mouse = std::make_unique<Mouse>();
    m_mouse->SetWindow(m_deviceResources->GetWindow());
    m_mouse->SetDpi(DisplayInformation::GetForCurrentView()->LogicalDpi);
    m_mouse->SetMode(Mouse::MODE_RELATIVE);

    m_gamepad = std::make_unique<GamePad>();
}

// Updates the application state once per frame.
void GameMain::Update()
{
    auto kbState = m_keyboard->GetState();
    auto mState = m_mouse->GetState();
    auto gpState = m_gamepad->GetState(0);

    // Update scene objects.
    m_timer.Tick([&]()
    {
        float elapsed = (float)m_timer.GetElapsedSeconds();

        float tRotateAmount = XMConvertToRadians(50.0f) * elapsed;
        float mRotateAmount = XMConvertToRadians(25.0f) * elapsed;
        float kRotateAmount = XMConvertToRadians(80.0f) * elapsed;
        float moveAmount = 5.0f * elapsed;
        float cameraMoveAmount = 3.0f * elapsed;

        //if (gpState.IsConnected())
        {
            DirectX::GamePad::ButtonStateTracker buttons;
            buttons.Update(gpState);

            DirectX::GamePad::ThumbSticks thumbs = gpState.thumbSticks;
            float leftThumbsX = moveAmount * thumbs.leftX;
            float leftThumbsY = moveAmount * thumbs.leftY;
            float rightThumbsX = tRotateAmount * thumbs.rightX;
            float rightThumbsY = tRotateAmount * thumbs.rightY * -1;

            // movement
            m_camera->Move({ leftThumbsX, 0.0f, leftThumbsY });
            // rotation
            m_camera->Rotate({ rightThumbsY, rightThumbsX, 0.0f });
        }
        //else 
        {
            // need to use some value based on the dpi to have it transition with mouse speed
            if (mState.x != 0.0f)
            {
                m_camera->Rotate({ 0.0f, mState.x * mRotateAmount, 0.0f });
            }
            if (mState.y != 0.0f)
            {
                m_camera->Rotate({ mState.y * mRotateAmount, 0.0f, 0.0f });
            }
        }

        if (kbState.Up)
        {
            m_gameObject1->Move({ 0.0f, 0.0f, moveAmount });
            m_gameObject2D1->Move({ 0.0f, 0.0f, moveAmount });
        }
        if (kbState.Down)
        {
            m_gameObject1->Move({ 0.0f, 0.0f, -moveAmount });
            m_gameObject2D1->Move({ 0.0f, 0.0f, -moveAmount });
        }
        if (kbState.Left)
        {
            m_gameObject1->Rotate({ 0.0f, -kRotateAmount, 0.0f });
            m_gameObject2D1->Rotate({ 0.0f, -kRotateAmount, 0.0f });
        }
        if (kbState.Right)
        {
            m_gameObject1->Rotate({ 0.0f, kRotateAmount, 0.0f });
            m_gameObject2D1->Rotate({ 0.0f, kRotateAmount, 0.0f });
        }
        if (kbState.Q)
        {
            m_gameObject1->Rotate({ kRotateAmount, 0.0f, 0.0f });
        }
        if (kbState.E)
        {
            m_gameObject1->Rotate({ -kRotateAmount, 0.0f, 0.0f });
        }


        if (kbState.Space)
        {
            m_camera->Move({ 0.0f, cameraMoveAmount, 0.0f });
        }

        if (kbState.W)
        {
            m_camera->Move({ 0.0f, 0.0f, cameraMoveAmount });
        }
        if (kbState.S)
        {
            m_camera->Move({ 0.0f, 0.0f, -cameraMoveAmount });
        }
        if (kbState.A)
        {
            m_camera->Move({ -cameraMoveAmount, 0.0f, 0.0f });
        }
        if (kbState.D)
        {
            m_camera->Move({ cameraMoveAmount, 0.0f, 0.0f });
        }

        if (kbState.T)
        {
            m_terrain->ToggleWireframe();
        }

        if (kbState.Escape) {
            exit(0);
        }

        // cam height from terrain
        float camHeight = m_terrain->GetHeightAt(m_camera->GetPosition().x, m_camera->GetPosition().z) + 1.0f;
        m_camera->SetPosition({ m_camera->GetPosition().x, camHeight, m_camera->GetPosition().z });


        // TEST COLLISION
        const std::shared_ptr<BoundingVolume> bv1 = m_gameObject2D1->GetBoundingVolume();
        const std::shared_ptr<BoundingVolume> bv2 = m_gameObject2D2->GetBoundingVolume();
        IntersectionType response2D = bv1->Intersects(bv2);

        const std::shared_ptr<BoundingVolume> bv3 = m_gameObject1->GetBoundingVolume();
        const std::shared_ptr<BoundingVolume> bv4 = m_gameObject2->GetBoundingVolume();
        IntersectionType response3D = bv3->Intersects(bv4);

        std::wstring debugText =
            m_camera->GetDiagText() +
            L"\nMouse: " + std::to_wstring(mState.x) + L", " + std::to_wstring(mState.y) +
            L"\nGampad: Left X: " + std::to_wstring(gpState.thumbSticks.leftX) +
            L", Y: " + std::to_wstring(gpState.thumbSticks.leftY) +
            L", Right X: " + std::to_wstring(gpState.thumbSticks.rightX) +
            L", Y: " + std::to_wstring(gpState.thumbSticks.rightY) +
            L"\nFPS: " + std::to_wstring(m_timer.GetFramesPerSecond()) + L" - " + std::to_wstring(m_timer.GetElapsedSeconds()) +
            L"\nTerrain Height: " + std::to_wstring(camHeight);
        if (response2D != NONE)
        {
            debugText += L"\n2D COLLISION";
        }
        if (response3D != NONE)
        {
            debugText += L"\n3D COLLISION";
        }


        m_camera->Update(m_timer);
        m_gameObject1->Update(m_timer);
        m_gameObject2->Update(m_timer);
        m_gameObject2D1->Update(m_timer);
        m_gameObject2D2->Update(m_timer);
        //m_sprite->Update(m_timer);

        m_textRenderer->SetText(debugText);
        m_textRenderer->Update(m_timer);
    });
}

// Renders the current frame according to the current application state.
// Returns true if the frame was rendered and is ready to be displayed.
bool GameMain::Render()
{
    // Don't try to render anything before the first Update.
    if (m_timer.GetFrameCount() == 0)
    {
        return false;
    }

    auto context = m_deviceResources->GetD3DDeviceContext();

    // Reset the viewport to target the whole screen.
    auto viewport = m_deviceResources->GetScreenViewport();
    context->RSSetViewports(1, &viewport);

    // Reset render targets to the screen.
    ID3D11RenderTargetView* const targets[1] = { m_deviceResources->GetBackBufferRenderTargetView() };
    context->OMSetRenderTargets(1, targets, m_deviceResources->GetDepthStencilView());

    // Clear the back buffer and depth stencil view.
    context->ClearRenderTargetView(
        m_deviceResources->GetBackBufferRenderTargetView(), DirectX::Colors::CornflowerBlue);
    context->ClearDepthStencilView(
        m_deviceResources->GetDepthStencilView(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

    // Render the scene objects.
    m_terrain->Render(m_camera->GetViewMatrix(), m_camera->GetProjectionMatrix());

    m_gameObject1->Render(m_camera->GetViewMatrix(), m_camera->GetProjectionMatrix());
    m_gameObject2->Render(m_camera->GetViewMatrix(), m_camera->GetProjectionMatrix());
    m_gameObject2D1->Render(m_camera->GetViewMatrix(), m_camera->GetProjectionMatrix());
    m_gameObject2D2->Render(m_camera->GetViewMatrix(), m_camera->GetProjectionMatrix());

    m_textRenderer->Render();

    return true;
}

// Notifies renderers that device resources need to be released.
void GameMain::OnDeviceLost()
{
    m_gameObject1->ReleaseDeviceDependentResources();
    m_gameObject2->ReleaseDeviceDependentResources();
    m_gameObject2D1->ReleaseDeviceDependentResources();
    m_gameObject2D2->ReleaseDeviceDependentResources();

    m_textRenderer->ReleaseDeviceDependentResources();
}

// Notifies renderers that device resources may now be recreated.
void GameMain::OnDeviceRestored()
{
    m_gameObject1->CreateDeviceDependentResources();
    m_gameObject2->CreateDeviceDependentResources();
    m_gameObject2D1->CreateDeviceDependentResources();
    m_gameObject2D2->CreateDeviceDependentResources();

    m_textRenderer->CreateDeviceDependentResources();

    CreateWindowSizeDependentResources();
}
