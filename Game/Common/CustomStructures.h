﻿#pragma once

using namespace DirectX;

namespace KerpEngine
{
    // Constant buffer used to send MVP matrices to the vertex shader.
    struct ModelViewProjectionConstantBuffer
    {
        XMFLOAT4X4 model;
        XMFLOAT4X4 view;
        XMFLOAT4X4 projection;
        XMFLOAT4 lightDirection;
        XMFLOAT4 lightColor;
        XMFLOAT4 outputColor;
    };

    // Used to send per-vertex data to the vertex shader.
    struct VertexPositionColored
    {
        XMFLOAT3 pos;
        XMFLOAT3 color;
    };

    struct FaceNormalIndexed
    {
        int32_t tl;
        int32_t tr;
        int32_t bl;
        int32_t br;
        float maxHeight;
        float minHeight;
        XMFLOAT3 normal;
    };

    struct Vertex {
        XMFLOAT3 position;
        XMFLOAT3 color;
        XMFLOAT3 normal;
        XMFLOAT2 texture0;
        //XMFLOAT2 texture1;
    };
}