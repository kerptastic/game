#include "pch.h"
#include "TestObject3D.h"

#include "..\Common\DirectXHelper.h"
#include "..\Engine\BoundingVolume.h"
#include "..\Engine\OABB.h"

using namespace Game;
using namespace KerpEngine;

using namespace DirectX;
using namespace Windows::Foundation;

// Initializes the test object with a given position
TestObject3D::TestObject3D(
    const std::shared_ptr<DX::DeviceResources>& deviceResources,
    const XMFLOAT3 position) :
    GameObject(deviceResources, position)
{
    Initialize();
}

// Public destructor.
TestObject3D::~TestObject3D()
{
}

// Initializes the object and its bounding volume.
void TestObject3D::Initialize()
{
    m_boundingVolume = std::make_shared<OABB>(
        m_deviceResources,
        m_position,
        XMFLOAT3{ 0.5f, 0.5f, 0.5f },
        XMFLOAT3{ -0.5f, -0.5f, -0.5f }
    );

    CreateDeviceDependentResources();
    CreateWindowSizeDependentResources();
}

// Called once per frame, rotates the cube and calculates the model and view matrices.
void TestObject3D::Update(DX::StepTimer const& timer)
{
    GameObject::Update(timer);
}

// Renders one frame using the vertex and pixel shaders.
void TestObject3D::Render(const XMMATRIX view, const XMMATRIX projection)
{
    // Loading is asynchronous. Only draw geometry after it's loaded.
    if (!m_loadingComplete)
    {
        return;
    }

    XMMATRIX model =
        XMMatrixTranspose(XMMatrixScalingFromVector(XMLoadFloat3(&m_scale))) *
        XMMatrixTranspose(XMMatrixTranslationFromVector(XMLoadFloat3(&m_position))) *
        XMMatrixTranspose(XMMatrixRotationRollPitchYawFromVector(XMLoadFloat3(&m_rotation)));

    // Prepare to pass the updated model matrix to the shader
    XMStoreFloat4x4(&m_constantBufferData.model, model);
    XMStoreFloat4x4(&m_constantBufferData.view, view);
    XMStoreFloat4x4(&m_constantBufferData.projection, projection);

    auto context = m_deviceResources->GetD3DDeviceContext();

    // Prepare the constant buffer to send it to the graphics device.
    context->UpdateSubresource1(m_constantBuffer.Get(), 0, NULL, &m_constantBufferData, 0, 0, 0);

    // Each vertex is one instance of the Vertex struct.
    UINT stride = sizeof(Vertex);
    UINT offset = 0;
    context->IASetVertexBuffers(0, 1, m_vertexBuffer.GetAddressOf(), &stride, &offset);

    context->IASetIndexBuffer(m_indexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0);
    context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    context->IASetInputLayout(m_inputLayout.Get());

    // Attach our vertex shader.
    context->VSSetShader(m_vertexShader.Get(), nullptr, 0);

    // Send the constant buffer to the graphics device.
    context->VSSetConstantBuffers1(0, 1, m_constantBuffer.GetAddressOf(), nullptr, nullptr);

    // Attach our pixel shader.
    context->PSSetShader(m_pixelShader.Get(), nullptr, 0);

    // Draw the objects.
    context->DrawIndexed(m_indexCount, 0, 0);

    // TODO: make rendering the bounding volume optional as part of debug mode
    m_boundingVolume->Render(view, projection);
}

// Creates the device dependent resources for the test object.
void TestObject3D::CreateDeviceDependentResources()
{
    // Load shaders asynchronously.
    auto loadVSTask = DX::ReadDataAsync(L"SampleVertexShader.cso");
    auto loadPSTask = DX::ReadDataAsync(L"SamplePixelShader.cso");

    // After the vertex shader file is loaded, create the shader and input layout.
    auto createVSTask = loadVSTask.then([this](const std::vector<byte>& fileData) {
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateVertexShader(&fileData[0], fileData.size(), nullptr, &m_vertexShader)
        );

        static const D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        };

        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateInputLayout(vertexDesc, ARRAYSIZE(vertexDesc), &fileData[0], fileData.size(), &m_inputLayout)
        );
    });

    // After the pixel shader file is loaded, create the shader and constant buffer.
    auto createPSTask = loadPSTask.then([this](const std::vector<byte>& fileData) {
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreatePixelShader(&fileData[0], fileData.size(), nullptr, &m_pixelShader)
        );

        CD3D11_BUFFER_DESC constantBufferDesc(sizeof(ModelViewProjectionConstantBuffer), D3D11_BIND_CONSTANT_BUFFER);
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateBuffer(&constantBufferDesc, nullptr, &m_constantBuffer)
        );
    });

    // Once both shaders are loaded, create the mesh.
    auto createCubeTask = (createPSTask && createVSTask).then([this]() {

        // Load mesh vertices. Each vertex has a position and a color.
        static const Vertex cubeVertices[] =
        {
            // front bottom left
            {
                {-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, 0.0f}
            },
            // front top left
            {
                {-0.5f, 0.5f,  -0.5f}, {0.0f, 0.0f, 1.0f}
            },
            // front top right
            {
                {0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}
            },
            // front bottom right
            {
                {0.5f, -0.5f, -0.5f}, {0.0f, 1.0f, 1.0f}
            },
            // back bottom left
            {
                {-0.5f, -0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}
            },
            // back top left
            {
                {-0.5f, 0.5f, 0.5f}, {1.0f, 0.0f, 1.0f}
            },
            // back top right
            {
                {0.5f, 0.5f, 0.5f}, {1.0f, 1.0f, 0.0f}
            },
            // back bottom right
            {
                {0.5f, -0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}
            },
        };

        D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
        vertexBufferData.pSysMem = cubeVertices;
        vertexBufferData.SysMemPitch = 0;
        vertexBufferData.SysMemSlicePitch = 0;
        CD3D11_BUFFER_DESC vertexBufferDesc(sizeof(cubeVertices), D3D11_BIND_VERTEX_BUFFER);
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &m_vertexBuffer)
        );

        // clockwise winding of vertices
        static const uint16_t cubeIndices[] =
        {
            // front
            0,1,2,
            0,2,3,
            // back
            4,6,5,
            4,7,6,
            // left
            4,5,1,
            4,1,0,
            // right
            3,2,6,
            3,6,7,
            // top
            1,5,6,
            1,6,2,
            // bottom
            4,0,3,
            4,3,7
        };

        m_indexCount = ARRAYSIZE(cubeIndices);

        D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
        indexBufferData.pSysMem = cubeIndices;
        indexBufferData.SysMemPitch = 0;
        indexBufferData.SysMemSlicePitch = 0;
        CD3D11_BUFFER_DESC indexBufferDesc(sizeof(cubeIndices), D3D11_BIND_INDEX_BUFFER);
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &m_indexBuffer)
        );
    });

    // Once the cube is loaded, the object is ready to be rendered.
    createCubeTask.then([this]() {
        m_loadingComplete = true;
    });
}

// Release 3D resources.
void TestObject3D::ReleaseDeviceDependentResources()
{
    //TODO: manage sampler and texture here
}

// Initializes view parameters when the window size changes.
void TestObject3D::CreateWindowSizeDependentResources()
{
}
