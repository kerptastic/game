#include "pch.h"
#include "TestObject2D.h"
#include "..\Common\DirectXHelper.h"

#include "..\Engine\AABB.h"
#include "..\Engine\BoundingVolume.h"


using namespace Game;

// Initializes the test object with a given position.
TestObject2D::TestObject2D(
    const std::shared_ptr<DX::DeviceResources>& deviceResources,
    const XMFLOAT3 position,
    ComPtr<ID3D11ShaderResourceView>& texture) :
    GameObject(deviceResources, position),
    m_texture(texture)
{
    Initialize();
}

// Public destructor.
TestObject2D::~TestObject2D()
{
}

// Initializes the object and its bounding volume.
void TestObject2D::Initialize()
{
    m_boundingVolume = std::make_shared<AABB>(
        m_deviceResources,
        m_position,
        XMFLOAT3{ 0.5f, 0.5f, 0.0f },
        XMFLOAT3{ -0.5f, -0.5f, 0.0f }
    );

    CreateDeviceDependentResources();
    CreateWindowSizeDependentResources();
}

// Called once per frame, rotates the cube and calculates the model and view matrices.
void TestObject2D::Update(DX::StepTimer const& timer)
{
    GameObject::Update(timer);
}

// Renders one frame using the vertex and pixel shaders.
void TestObject2D::Render(const XMMATRIX view, const XMMATRIX projection)
{
    // Loading is asynchronous. Only draw geometry after it's loaded.
    if (!m_loadingComplete)
    {
        return;
    }

    XMMATRIX model =
        XMMatrixTranspose(XMMatrixScalingFromVector(XMLoadFloat3(&m_scale))) *
        XMMatrixTranspose(XMMatrixTranslationFromVector(XMLoadFloat3(&m_position))) *
        XMMatrixTranspose(XMMatrixRotationRollPitchYawFromVector(XMLoadFloat3(&m_rotation)));

    // Prepare to pass the updated model matrix to the shader
    XMStoreFloat4x4(&m_constantBufferData.model, model);
    XMStoreFloat4x4(&m_constantBufferData.view, view);
    XMStoreFloat4x4(&m_constantBufferData.projection, projection);

    auto context = m_deviceResources->GetD3DDeviceContext();

    // Prepare the constant buffer to send it to the graphics device.
    context->UpdateSubresource1(m_constantBuffer.Get(), 0, NULL, &m_constantBufferData, 0, 0, 0);

    // Each vertex is one instance of the Vertex struct.
    UINT stride = sizeof(Vertex);
    UINT offset = 0;
    context->IASetVertexBuffers(0, 1, m_vertexBuffer.GetAddressOf(), &stride, &offset);

    context->IASetIndexBuffer(m_indexBuffer.Get(), DXGI_FORMAT_R16_UINT, 0);
    context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    context->IASetInputLayout(m_inputLayout.Get());

    // Attach our vertex shader.
    context->VSSetShader(m_vertexShader.Get(), nullptr, 0);

    // Send the constant buffer to the graphics device.
    context->VSSetConstantBuffers1(0, 1, m_constantBuffer.GetAddressOf(), nullptr, nullptr);

    // Attach our pixel shader.
    context->PSSetShader(m_pixelShader.Get(), nullptr, 0);
    context->PSSetShaderResources(0, 1, m_texture.GetAddressOf());
    context->PSSetSamplers(0, 1, m_sampler.GetAddressOf());

    // Draw the objects.
    context->DrawIndexed(m_indexCount, 0, 0);

    if (m_boundingVolume) {
        m_boundingVolume->Render(view, projection);
    }
}

// Creates the device dependent resources for the test object.
void TestObject2D::CreateDeviceDependentResources()
{
    // Load shaders asynchronously.
    auto loadVSTask = DX::ReadDataAsync(L"TexturedVertexShader.cso");
    auto loadPSTask = DX::ReadDataAsync(L"TexturedPixelShader.cso");
    auto loadTDTask = DX::ReadDataAsync(L"Content\\grass_1024x1024.jpg");

    // After the vertex shader file is loaded, create the shader and input layout.
    auto createVSTask = loadVSTask.then([this](const std::vector<byte>& fileData) {
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateVertexShader(
                &fileData[0],
                fileData.size(),
                nullptr,
                &m_vertexShader
            )
        );

        static const D3D11_INPUT_ELEMENT_DESC vertexDesc[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
            { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        };

        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateInputLayout(
                vertexDesc,
                ARRAYSIZE(vertexDesc),
                &fileData[0],
                fileData.size(),
                &m_inputLayout
            )
        );
    });

    // After the pixel shader file is loaded, create the shader and constant buffer.
    auto createPSTask = loadPSTask.then([this](const std::vector<byte>& fileData) {
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreatePixelShader(
                &fileData[0],
                fileData.size(),
                nullptr,
                &m_pixelShader
            )
        );

        CD3D11_BUFFER_DESC constantBufferDesc(sizeof(ModelViewProjectionConstantBuffer), D3D11_BIND_CONSTANT_BUFFER);
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateBuffer(
                &constantBufferDesc,
                nullptr,
                &m_constantBuffer
            )
        );
    });

    auto createTDTask = loadTDTask.then([this](const std::vector<byte>& vertexShaderBytecode) {
        D3D11_SAMPLER_DESC samplerDesc;
        ZeroMemory(&samplerDesc, sizeof(samplerDesc));

        samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

        // The sampler does not use anisotropic filtering, so this parameter is ignored.
        samplerDesc.MaxAnisotropy = 0;

        // Specify how texture coordinates outside of the range 0..1 are resolved.
        samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
        samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
        samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

        // Use no special MIP clamping or bias.
        samplerDesc.MipLODBias = 0.0f;
        samplerDesc.MinLOD = 0;
        samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

        // Don't use a comparison function.
        samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;

        // Border address mode is not used, so this parameter is ignored.
        samplerDesc.BorderColor[0] = 0.0f;
        samplerDesc.BorderColor[1] = 0.0f;
        samplerDesc.BorderColor[2] = 0.0f;
        samplerDesc.BorderColor[3] = 0.0f;

        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateSamplerState(
                &samplerDesc,
                &m_sampler
            )
        );
    });

    // Once both shaders are loaded, create the mesh.
    auto create2DFaceTask = (createPSTask && createVSTask && createTDTask).then([this]() {
        // Load mesh vertices. Each vertex has a position and a color.
        static const Vertex cubeVertices[] =
        {
            // front bottom left
            {
                {-0.5f, -0.5f, 0.0f}, {}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}
            },
            // front top left
            {
                {-0.5f, 0.5f, 0.0f}, {}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f}
            },
            // front top right
            {
                {0.5f,  0.5f, 0.0f}, {}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}
            },
            // front bottom right
            {
                {0.5f, -0.5f, 0.0f }, {}, {0.0f, 0.0f, 1.0f }, {1.0f, 1.0f}
            }
        };

        // Load mesh indices. Each trio of indices represents
        // a triangle to be rendered on the screen.
        // For example: 0,2,1 means that the vertices with indexes
        // 0, 2 and 1 from the vertex buffer compose the 
        // first triangle of this mesh.
        static const unsigned short faceIndices[] =
        {
            0, 1, 2,
            0, 2, 3,
        };

        D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
        vertexBufferData.pSysMem = cubeVertices;
        vertexBufferData.SysMemPitch = 0;
        vertexBufferData.SysMemSlicePitch = 0;

        CD3D11_BUFFER_DESC vertexBufferDesc(sizeof(cubeVertices), D3D11_BIND_VERTEX_BUFFER);
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateBuffer(
                &vertexBufferDesc,
                &vertexBufferData,
                &m_vertexBuffer
            )
        );

        m_indexCount = ARRAYSIZE(faceIndices);

        D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
        indexBufferData.pSysMem = faceIndices;
        indexBufferData.SysMemPitch = 0;
        indexBufferData.SysMemSlicePitch = 0;

        CD3D11_BUFFER_DESC indexBufferDesc(sizeof(faceIndices), D3D11_BIND_INDEX_BUFFER);
        DX::ThrowIfFailed(
            m_deviceResources->GetD3DDevice()->CreateBuffer(
                &indexBufferDesc,
                &indexBufferData,
                &m_indexBuffer
            )
        );
    });

    // Once the cube is loaded, the object is ready to be rendered.
    create2DFaceTask.then([this]() {
        m_loadingComplete = true;
    });
}

// Release 2D resources.
void TestObject2D::ReleaseDeviceDependentResources()
{
    //TODO: manage sampler and texture here
}

// Initializes view parameters when the window size changes.
void TestObject2D::CreateWindowSizeDependentResources()
{
}
