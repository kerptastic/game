#pragma once

#include "..\Engine\GameObject.h"

#include "..\Common\DeviceResources.h"
#include "..\Common\CustomStructures.h"
#include "..\Common\StepTimer.h"

using namespace DirectX;
using namespace KerpEngine;

namespace Game
{
    class TestObject2D : public GameObject
    {
    public:
        TestObject2D(
            const std::shared_ptr<DX::DeviceResources>& deviceResources,
            const XMFLOAT3 position,
            ComPtr<ID3D11ShaderResourceView>& texture);
        ~TestObject2D();

        void CreateDeviceDependentResources();
        void CreateWindowSizeDependentResources();
        void ReleaseDeviceDependentResources();

        void Update(DX::StepTimer const& timer);
        void Render(const XMMATRIX view, const XMMATRIX projection);

    private:
        void Initialize();

        ComPtr<ID3D11ShaderResourceView> m_texture;
        ComPtr<ID3D11SamplerState>       m_sampler;

    };
}
