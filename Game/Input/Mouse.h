#pragma once


// Required to access ABI::Windows Application Binary Interfaces (ABI)
#include <windows.applicationmodel.core.h>

using namespace Windows::UI::Core;

namespace Input
{
	// Other functions defined below the enum/struct
	class Mouse
	{
	public:
		Mouse();
		Mouse(Mouse&& moveFrom);
		Mouse& operator= (Mouse&& moveFrom);

		~Mouse();

		// Deleted
		Mouse(Mouse const&) = delete;
		// Deleted
		Mouse& operator= (Mouse const&) = delete;
	private:
		class Impl;
		std::unique_ptr<Impl> p_impl;

	public:
		enum Mode
		{
			MODE_ABSOLUTE = 0,
			MODE_RELATIVE,
		};

		struct State
		{
			bool    leftButton;
			bool    middleButton;
			bool    rightButton;
			bool    xButton1;
			bool    xButton2;
			int     x;
			int     y;
			int     scrollWheelValue;
			Mode    positionMode;
		};

		class ButtonStateTracker
		{
		public:
			enum ButtonState
			{
				UP = 0,         // Button is up
				HELD = 1,       // Button is held down
				RELEASED = 2,   // Button was just released
				PRESSED = 3,    // Buton was just pressed
			};

			ButtonState leftButton;
			ButtonState middleButton;
			ButtonState rightButton;
			ButtonState xButton1;
			ButtonState xButton2;

			ButtonStateTracker() { Reset(); }
			void __cdecl Update(const State& state);
			void __cdecl Reset();

			State __cdecl GetLastState() const { return lastState; }

		private:
			State lastState;
		};

		Mouse::State __cdecl GetState() const;
		void __cdecl ResetScrollWheelValue();
		void __cdecl SetMode(Mode mode);
		bool __cdecl IsConnected() const;
		void __cdecl SetDpi(float dpi);
		void __cdecl SetWindow(CoreWindow^ window);
		void __cdecl SetWindow(ABI::Windows::UI::Core::ICoreWindow* window);
	};
}
