#include "pch.h"
#include "Mouse.h"
#include "..\Common\PlatformHelpers.h"

#include <Windows.Devices.Input.h>
#include <windows.applicationmodel.core.h>

using namespace Input;
using namespace DirectX;

// Hidden implementation (PIMPL Idiom. See: https://cpppatterns.com/patterns/pimpl.html).
class Mouse::Impl
{
public:
	Impl(Mouse* owner) : m_owner(owner), m_dpi(96.f), m_mode(MODE_ABSOLUTE)
	{
		m_pointerPressedToken.value = 0;
		m_pointerReleasedToken.value = 0;
		m_pointerMovedToken.value = 0;
		m_pointerWheelToken.value = 0;
		m_pointerMouseMovedToken.value = 0;

		if (m_instance)
		{
			throw std::exception("Mouse is a singleton");
		}

		m_instance = this;

		memset(&m_state, 0, sizeof(Mouse::State));

		m_scrollWheelValue.reset(CreateEventEx(nullptr, nullptr, CREATE_EVENT_MANUAL_RESET, EVENT_MODIFY_STATE | SYNCHRONIZE));
		m_relativeRead.reset(CreateEventEx(nullptr, nullptr, CREATE_EVENT_MANUAL_RESET, EVENT_MODIFY_STATE | SYNCHRONIZE));
		
		if (!m_scrollWheelValue || !m_relativeRead)
		{
			throw std::exception("CreateEventEx");
		}
	}

	// Destructor - removes handlers from the Window.
	~Impl()
	{
		m_instance = nullptr;

		RemoveHandlers();
	}

	// Returns the current Mouse state.
	void GetState(Mouse::State& state) const
	{
		memcpy(&state, &m_state, sizeof(State));

		DWORD result = WaitForSingleObjectEx(m_scrollWheelValue.get(), 0, FALSE);
		if (result == WAIT_FAILED)
			throw std::exception("WaitForSingleObjectEx");

		if (result == WAIT_OBJECT_0)
		{
			state.scrollWheelValue = 0;
		}

		if (m_mode == MODE_RELATIVE)
		{
			result = WaitForSingleObjectEx(m_relativeRead.get(), 0, FALSE);

			if (result == WAIT_FAILED)
				throw std::exception("WaitForSingleObjectEx");

			if (result == WAIT_OBJECT_0)
			{
				state.x = 0;
				state.y = 0;
			}
			else
			{
				SetEvent(m_relativeRead.get());
			}
		}

		state.positionMode = m_mode;
	}

	// Resets the state value for the scroll wheel.
	void ResetScrollWheelValue()
	{
		SetEvent(m_scrollWheelValue.get());
	}

	// Sets the mode for the Mouse, either MODE_RELATIVE or MODE_ABSOLUTE.
	void SetMode(Mode mode)
	{
		using namespace Microsoft::WRL;
		using namespace Microsoft::WRL::Wrappers;
		using namespace ABI::Windows::UI::Core;
		using namespace ABI::Windows::Foundation;

		if (m_mode == mode)
			return;

		ComPtr<ABI::Windows::UI::Core::ICoreWindowStatic> statics;
		HRESULT hr = GetActivationFactory(HStringReference(RuntimeClass_Windows_UI_Core_CoreWindow).Get(), statics.GetAddressOf());
		ThrowIfFailed(hr);

		ComPtr<ABI::Windows::UI::Core::ICoreWindow> window;
		hr = statics->GetForCurrentThread(window.GetAddressOf());
		ThrowIfFailed(hr);

		if (mode == MODE_RELATIVE)
		{
			hr = window->get_PointerCursor(m_cursor.ReleaseAndGetAddressOf());
			ThrowIfFailed(hr);

			hr = window->put_PointerCursor(nullptr);
			ThrowIfFailed(hr);

			SetEvent(m_relativeRead.get());

			m_mode = MODE_RELATIVE;
		}
		else
		{
			if (!m_cursor)
			{
				ComPtr<ABI::Windows::UI::Core::ICoreCursorFactory> factory;
				hr = GetActivationFactory(HStringReference(RuntimeClass_Windows_UI_Core_CoreCursor).Get(), factory.GetAddressOf());
				ThrowIfFailed(hr);

				hr = factory->CreateCursor(CoreCursorType_Arrow, 0, m_cursor.GetAddressOf());
				ThrowIfFailed(hr);
			}

			hr = window->put_PointerCursor(m_cursor.Get());
			ThrowIfFailed(hr);

			m_cursor.Reset();

			m_mode = MODE_ABSOLUTE;
		}
	}

	// Whether or not the mouse is detected / connected.
	bool IsConnected() const
	{
		using namespace Microsoft::WRL;
		using namespace Microsoft::WRL::Wrappers;
		using namespace ABI::Windows::Devices::Input;
		using namespace ABI::Windows::Foundation;

		ComPtr<IMouseCapabilities> caps;
		HRESULT hr = RoActivateInstance(HStringReference(RuntimeClass_Windows_Devices_Input_MouseCapabilities).Get(), &caps);
		ThrowIfFailed(hr);

		INT32 value;
		if (SUCCEEDED(caps->get_MousePresent(&value)))
		{
			return value != 0;
		}

		return false;
	}

	// Sets the Window for the Mouse to capture events from, and initialized for mouse state reading.
	void SetWindow(ABI::Windows::UI::Core::ICoreWindow* window)
	{
		using namespace Microsoft::WRL;
		using namespace Microsoft::WRL::Wrappers;
		using namespace ABI::Windows::Foundation;
		using namespace ABI::Windows::Devices::Input;

		if (m_window.Get() == window)
			return;

		RemoveHandlers();

		m_window = window;

		if (!window)
		{
			m_cursor.Reset();
			m_mouse.Reset();
			return;
		}

		ComPtr<IMouseDeviceStatics> mouseStatics;
		HRESULT hr = GetActivationFactory(HStringReference(RuntimeClass_Windows_Devices_Input_MouseDevice).Get(), mouseStatics.GetAddressOf());
		ThrowIfFailed(hr);

		hr = mouseStatics->GetForCurrentView(m_mouse.ReleaseAndGetAddressOf());
		ThrowIfFailed(hr);

		typedef __FITypedEventHandler_2_Windows__CDevices__CInput__CMouseDevice_Windows__CDevices__CInput__CMouseEventArgs MouseMovedHandler;
		hr = m_mouse->add_MouseMoved(Callback<MouseMovedHandler>(MouseMovedEvent).Get(), &m_pointerMouseMovedToken);
		ThrowIfFailed(hr);

		typedef __FITypedEventHandler_2_Windows__CUI__CCore__CCoreWindow_Windows__CUI__CCore__CPointerEventArgs PointerHandler;
		auto cb = Callback<PointerHandler>(PointerEvent);

		hr = window->add_PointerPressed(cb.Get(), &m_pointerPressedToken);
		ThrowIfFailed(hr);

		hr = window->add_PointerReleased(cb.Get(), &m_pointerReleasedToken);
		ThrowIfFailed(hr);

		hr = window->add_PointerMoved(cb.Get(), &m_pointerMovedToken);
		ThrowIfFailed(hr);

		hr = window->add_PointerWheelChanged(Callback<PointerHandler>(PointerWheel).Get(), &m_pointerWheelToken);
		ThrowIfFailed(hr);
	}

	// Sets the DPI for the mouse.
	void SetDPI(float dpi)
	{
		m_dpi = dpi;
	}

	ScopedHandle    m_scrollWheelValue;
	ScopedHandle    m_relativeRead;
	Mouse::Mode     m_mode;
	Mouse::State    m_state;
	Mouse*          m_owner;
	float           m_dpi;
	static Mouse::Impl* m_instance;
private:

	Microsoft::WRL::ComPtr<ABI::Windows::UI::Core::ICoreWindow> m_window;
	Microsoft::WRL::ComPtr<ABI::Windows::Devices::Input::IMouseDevice> m_mouse;
	Microsoft::WRL::ComPtr<ABI::Windows::UI::Core::ICoreCursor> m_cursor;

	EventRegistrationToken m_pointerPressedToken;
	EventRegistrationToken m_pointerReleasedToken;
	EventRegistrationToken m_pointerMovedToken;
	EventRegistrationToken m_pointerWheelToken;
	EventRegistrationToken m_pointerMouseMovedToken;

	// Removes handlers from the Window.
	void RemoveHandlers()
	{
		if (m_window)
		{
			(void)m_window->remove_PointerPressed(m_pointerPressedToken);
			m_pointerPressedToken.value = 0;

			(void)m_window->remove_PointerReleased(m_pointerReleasedToken);
			m_pointerReleasedToken.value = 0;

			(void)m_window->remove_PointerMoved(m_pointerMovedToken);
			m_pointerMovedToken.value = 0;

			(void)m_window->remove_PointerWheelChanged(m_pointerWheelToken);
			m_pointerWheelToken.value = 0;
		}

		if (m_mouse)
		{
			(void)m_mouse->remove_MouseMoved(m_pointerMouseMovedToken);
			m_pointerMouseMovedToken.value = 0;
		}
	}

	// 
	static HRESULT PointerEvent(IInspectable *, ABI::Windows::UI::Core::IPointerEventArgs*args)
	{
		using namespace Microsoft::WRL;
		using namespace ABI::Windows::Foundation;
		using namespace ABI::Windows::UI::Input;
		using namespace ABI::Windows::Devices::Input;

		if (!m_instance)
			return S_OK;

		ComPtr<IPointerPoint> currentPoint;
		HRESULT hr = args->get_CurrentPoint(currentPoint.GetAddressOf());
		ThrowIfFailed(hr);

		ComPtr<IPointerDevice> pointerDevice;
		hr = currentPoint->get_PointerDevice(pointerDevice.GetAddressOf());
		ThrowIfFailed(hr);

		PointerDeviceType devType;
		hr = pointerDevice->get_PointerDeviceType(&devType);
		ThrowIfFailed(hr);

		if (devType == PointerDeviceType::PointerDeviceType_Mouse)
		{
			ComPtr<IPointerPointProperties> props;
			hr = currentPoint->get_Properties(props.GetAddressOf());
			ThrowIfFailed(hr);

			boolean value;
			hr = props->get_IsLeftButtonPressed(&value);
			ThrowIfFailed(hr);
			m_instance->m_state.leftButton = value != 0;

			hr = props->get_IsRightButtonPressed(&value);
			ThrowIfFailed(hr);
			m_instance->m_state.rightButton = value != 0;

			hr = props->get_IsMiddleButtonPressed(&value);
			ThrowIfFailed(hr);
			m_instance->m_state.middleButton = value != 0;

			hr = props->get_IsXButton1Pressed(&value);
			ThrowIfFailed(hr);
			m_instance->m_state.xButton1 = value != 0;

			hr = props->get_IsXButton2Pressed(&value);
			ThrowIfFailed(hr);
			m_instance->m_state.xButton2 = value != 0;
		}

		if (m_instance->m_mode == MODE_ABSOLUTE)
		{
			Point pos;
			hr = currentPoint->get_Position(&pos);
			ThrowIfFailed(hr);

			float dpi = m_instance->m_dpi;

			m_instance->m_state.x = static_cast<int>(pos.X * dpi / 96.f + 0.5f);
			m_instance->m_state.y = static_cast<int>(pos.Y * dpi / 96.f + 0.5f);
		}

		return S_OK;
	}

	//
	static HRESULT PointerWheel(IInspectable *, ABI::Windows::UI::Core::IPointerEventArgs*args)
	{
		using namespace ABI::Windows::Foundation;
		using namespace ABI::Windows::UI::Input;
		using namespace ABI::Windows::Devices::Input;

		if (!m_instance)
			return S_OK;

		Microsoft::WRL::ComPtr<IPointerPoint> currentPoint;
		HRESULT hr = args->get_CurrentPoint(currentPoint.GetAddressOf());
		ThrowIfFailed(hr);

		Microsoft::WRL::ComPtr<IPointerDevice> pointerDevice;
		hr = currentPoint->get_PointerDevice(pointerDevice.GetAddressOf());
		ThrowIfFailed(hr);

		PointerDeviceType devType;
		hr = pointerDevice->get_PointerDeviceType(&devType);
		ThrowIfFailed(hr);

		if (devType == PointerDeviceType::PointerDeviceType_Mouse)
		{
			Microsoft::WRL::ComPtr<IPointerPointProperties> props;
			hr = currentPoint->get_Properties(props.GetAddressOf());
			ThrowIfFailed(hr);

			INT32 value;
			hr = props->get_MouseWheelDelta(&value);
			ThrowIfFailed(hr);

			HANDLE evt = m_instance->m_scrollWheelValue.get();
			if (WaitForSingleObjectEx(evt, 0, FALSE) == WAIT_OBJECT_0)
			{
				m_instance->m_state.scrollWheelValue = 0;
				ResetEvent(evt);
			}

			m_instance->m_state.scrollWheelValue += value;

			if (m_instance->m_mode == MODE_ABSOLUTE)
			{
				Point pos;
				hr = currentPoint->get_Position(&pos);
				ThrowIfFailed(hr);

				float dpi = m_instance->m_dpi;

				m_instance->m_state.x = static_cast<int>(pos.X * dpi / 96.f + 0.5f);
				m_instance->m_state.y = static_cast<int>(pos.Y * dpi / 96.f + 0.5f);
			}
		}

		return S_OK;
	}

	//
	static HRESULT MouseMovedEvent(IInspectable *, ABI::Windows::Devices::Input::IMouseEventArgs* args)
	{
		using namespace ABI::Windows::Devices::Input;

		if (!m_instance)
			return S_OK;

		if (m_instance->m_mode == MODE_RELATIVE)
		{
			MouseDelta delta;
			HRESULT hr = args->get_MouseDelta(&delta);
			ThrowIfFailed(hr);

			m_instance->m_state.x = delta.X;
			m_instance->m_state.y = delta.Y;

			ResetEvent(m_instance->m_relativeRead.get());
		}

		return S_OK;
	}
};

// Static instance initialization.
Mouse::Impl* Mouse::Impl::m_instance = nullptr;

// Public constructor.
Mouse::Mouse() : p_impl(new Impl(this))
{
}

// Public destructor.
Mouse::~Mouse()
{
}

// Move constructor.
Mouse::Mouse(Mouse&& moveFrom) : p_impl(std::move(moveFrom.p_impl))
{
	p_impl->m_owner = this;
}

// Move assignment.
Mouse& Mouse::operator= (Mouse&& moveFrom)
{
	p_impl = std::move(moveFrom.p_impl);
	p_impl->m_owner = this;

	return *this;
}

// Sets the ICoreWindow to pull events from.
void __cdecl Mouse::SetWindow(ABI::Windows::UI::Core::ICoreWindow* window)
{
	p_impl->SetWindow(window);
}

// Reinterprets the CoreWindow into an ICoreWindow, which will set the window to pull events from.
void __cdecl Mouse::SetWindow(Windows::UI::Core::CoreWindow^ window)
{
	SetWindow(reinterpret_cast<ABI::Windows::UI::Core::ICoreWindow*>(window));
}

// Retrieve the current state of the mouse
Mouse::State __cdecl Mouse::GetState() const
{
	Mouse::State state;
	p_impl->GetState(state);
	return state;
}

// Resets the accumulated scroll wheel value
void __cdecl Mouse::ResetScrollWheelValue()
{
	p_impl->ResetScrollWheelValue();
}

// Sets mouse mode (defaults to absolute)
void __cdecl Mouse::SetMode(Mouse::Mode mode)
{
	p_impl->SetMode(mode);
}

// Feature detection
bool __cdecl Mouse::IsConnected() const
{
	return p_impl->IsConnected();
}

// Sets the DPI for the mouse.
void Mouse::SetDpi(float dpi)
{
	p_impl->SetDPI(dpi);
}

#define UPDATE_BUTTON_STATE(field) field = static_cast<ButtonState>( ( !!state.field ) | ( ( !!state.field ^ !!lastState.field ) << 1 ) );

// Updates the Button State Tracker.
void Mouse::ButtonStateTracker::Update(const Mouse::State& state)
{
	UPDATE_BUTTON_STATE(leftButton);

	assert((!state.leftButton && !lastState.leftButton) == (leftButton == UP));
	assert((state.leftButton && lastState.leftButton) == (leftButton == HELD));
	assert((!state.leftButton && lastState.leftButton) == (leftButton == RELEASED));
	assert((state.leftButton && !lastState.leftButton) == (leftButton == PRESSED));

	UPDATE_BUTTON_STATE(middleButton);
	UPDATE_BUTTON_STATE(rightButton);
	UPDATE_BUTTON_STATE(xButton1);
	UPDATE_BUTTON_STATE(xButton2);

	lastState = state;
}

#undef UPDATE_BUTTON_STATE

// Resets the Button State Tracker.
void Mouse::ButtonStateTracker::Reset()
{
	memset(this, 0, sizeof(ButtonStateTracker));
}