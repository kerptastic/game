#include "pch.h"
#include "Keyboard.h"
#include "..\Common\PlatformHelpers.h"

#include <Windows.Devices.Input.h>
#include <windows.applicationmodel.core.h>

using namespace Input;
using namespace DirectX;

static_assert(sizeof(Keyboard::State) == (256 / 8), "Size mismatch for State");

namespace Input
{
	// Key Down storage function
	void KeyDown(int key, Keyboard::State& state)
	{
		if (key < 0 || key > 0xfe)
			return;

		auto ptr = reinterpret_cast<uint32_t*>(&state);
		unsigned int bf = 1u << (key & 0x1f);

		ptr[(key >> 5)] |= bf;
	}

	// Key Up storage function
	void KeyUp(int key, Keyboard::State& state)
	{
		if (key < 0 || key > 0xfe)
			return;

		auto ptr = reinterpret_cast<uint32_t*>(&state);
		unsigned int bf = 1u << (key & 0x1f);

		ptr[(key >> 5)] &= ~bf;
	}
}

// Hidden implementation (PIMPL Idiom. See: https://cpppatterns.com/patterns/pimpl.html).
class Keyboard::Impl
{
public:
	// Creates a new private implementation of the Keyboard logic.
	Impl(Keyboard* owner) : m_owner(owner)
	{
		if (m_instance)
			throw std::exception("Keyboard is a singleton");

		m_instance = this;

		memset(&m_state, 0, sizeof(State));
	}

	// Destructor - removes handlers from the Window.
	~Impl()
	{
		m_instance = nullptr;

		RemoveHandlers();
	}

	// Returns the current Keyboard KeyPress state.
	void GetState(State& state) const
	{
		memcpy(&state, &m_state, sizeof(State));
	}

	// Whether or not the Keyboard is detected / connected.
	bool IsConnected() const
	{
		using namespace Microsoft::WRL;
		using namespace Microsoft::WRL::Wrappers;
		using namespace ABI::Windows::Devices::Input;
		using namespace ABI::Windows::Foundation;

		ComPtr<IKeyboardCapabilities> caps;
		HRESULT hr = RoActivateInstance(HStringReference(RuntimeClass_Windows_Devices_Input_KeyboardCapabilities).Get(), &caps);

		ThrowIfFailed(hr);

		INT32 value;

		if (SUCCEEDED(caps->get_KeyboardPresent(&value)))
		{
			return value != 0;
		}

		return false;
	}

	// Resets the Keyboard KeyPress data.
	void Reset()
	{
		memset(&m_state, 0, sizeof(State));
	}

	// Sets the Window for the Keyboard to capture events from, and initialized for key reading.
	void SetWindow(ABI::Windows::UI::Core::ICoreWindow* window)
	{
		using namespace Microsoft::WRL;
		using namespace Microsoft::WRL::Wrappers;
		using namespace ABI::Windows::UI::Core;

		if (m_window.Get() == window)
			return;

		RemoveHandlers();

		m_window = window;

		if (!window)
			return;

		typedef __FITypedEventHandler_2_Windows__CUI__CCore__CCoreWindow_Windows__CUI__CCore__CWindowActivatedEventArgs ActivatedHandler;
		HRESULT hr = window->add_Activated(Callback<ActivatedHandler>(Activated).Get(), &m_activatedToken);
		ThrowIfFailed(hr);

		ComPtr<ABI::Windows::UI::Core::ICoreDispatcher> dispatcher;
		hr = window->get_Dispatcher(dispatcher.GetAddressOf());
		ThrowIfFailed(hr);

		ComPtr<ABI::Windows::UI::Core::ICoreAcceleratorKeys> keys;
		hr = dispatcher.As(&keys);
		ThrowIfFailed(hr);

		typedef __FITypedEventHandler_2_Windows__CUI__CCore__CCoreDispatcher_Windows__CUI__CCore__CAcceleratorKeyEventArgs AcceleratorKeyHandler;
		hr = keys->add_AcceleratorKeyActivated(Callback<AcceleratorKeyHandler>(AcceleratorKeyEvent).Get(), &m_acceleratorKeyToken);
		ThrowIfFailed(hr);
	}

	State	               m_state;
	Keyboard*			   m_owner;
	static Keyboard::Impl* m_instance;

private:
	Microsoft::WRL::ComPtr<ABI::Windows::UI::Core::ICoreWindow> m_window;
	EventRegistrationToken m_acceleratorKeyToken;
	EventRegistrationToken m_activatedToken;

	// Remove the Keyboard Handlers
	void RemoveHandlers()
	{
		if (m_window)
		{
			using namespace Microsoft::WRL;
			using namespace ABI::Windows::UI::Core;

			ComPtr<ABI::Windows::UI::Core::ICoreDispatcher> dispatcher;
			HRESULT hr = m_window->get_Dispatcher(dispatcher.GetAddressOf());
			ThrowIfFailed(hr);

			(void)m_window->remove_Activated(m_activatedToken);
			m_acceleratorKeyToken.value = 0;

			ComPtr<ABI::Windows::UI::Core::ICoreAcceleratorKeys> keys;
			hr = dispatcher.As(&keys);
			ThrowIfFailed(hr);

			(void)keys->remove_AcceleratorKeyActivated(m_acceleratorKeyToken);
			m_acceleratorKeyToken.value = 0;
		}
	}

	// When the Keyboard is Activated, will clear out the keys
	static HRESULT Activated(IInspectable *, ABI::Windows::UI::Core::IWindowActivatedEventArgs*)
	{
		auto pImpl = Impl::m_instance;

		if (!pImpl)
			return S_OK;

		pImpl->Reset();

		return S_OK;
	}

	// Read and Store Key Events.
	static HRESULT AcceleratorKeyEvent(IInspectable *, ABI::Windows::UI::Core::IAcceleratorKeyEventArgs* args)
	{
		using namespace ABI::Windows::System;
		using namespace ABI::Windows::UI::Core;

		auto pImpl = Impl::m_instance;

		if (!pImpl)
			return S_OK;

		ABI::Windows::UI::Core::CoreAcceleratorKeyEventType evtType;
		HRESULT hr = args->get_EventType(&evtType);
		ThrowIfFailed(hr);

		bool down = false;

		switch (evtType)
		{
		case CoreAcceleratorKeyEventType_KeyDown:
		case CoreAcceleratorKeyEventType_SystemKeyDown:
			down = true;
			break;

		case CoreAcceleratorKeyEventType_KeyUp:
		case CoreAcceleratorKeyEventType_SystemKeyUp:
			break;

		default:
			return S_OK;
		}

		ABI::Windows::UI::Core::CorePhysicalKeyStatus status;
		hr = args->get_KeyStatus(&status);
		ThrowIfFailed(hr);

		VirtualKey virtualKey;
		hr = args->get_VirtualKey(&virtualKey);
		ThrowIfFailed(hr);

		int vk = static_cast<int>(virtualKey);

		switch (vk)
		{
		case VK_SHIFT:
			vk = (status.ScanCode == 0x36) ? VK_RSHIFT : VK_LSHIFT;
			if (!down)
			{
				// Workaround to ensure left vs. right shift get cleared when both were pressed at same time
				KeyUp(VK_LSHIFT, pImpl->m_state);
				KeyUp(VK_RSHIFT, pImpl->m_state);
			}
			break;

		case VK_CONTROL:
			vk = (status.IsExtendedKey) ? VK_RCONTROL : VK_LCONTROL;
			break;

		case VK_MENU:
			vk = (status.IsExtendedKey) ? VK_RMENU : VK_LMENU;
			break;
		}

		if (down)
		{
			KeyDown(vk, pImpl->m_state);
		}
		else
		{
			KeyUp(vk, pImpl->m_state);
		}

		return S_OK;
	}
};

// Static instance initialization.
Keyboard::Impl* Keyboard::Impl::m_instance = nullptr;

// Public constructor
Keyboard::Keyboard() : p_impl(new Impl(this))
{
}

// Public destructor.
Keyboard::~Keyboard()
{
}

// Move constructor.
Keyboard::Keyboard(Keyboard&& moveFrom) : p_impl(std::move(moveFrom.p_impl))
{
	p_impl->m_owner = this;
}

// Move assignment.
Keyboard& Keyboard::operator= (Keyboard&& moveFrom)
{
	p_impl = std::move(moveFrom.p_impl);
	p_impl->m_owner = this;

	return *this;
}

// Sets the ICoreWindow to pull events from.
void __cdecl Keyboard::SetWindow(ABI::Windows::UI::Core::ICoreWindow* window)
{
	p_impl->SetWindow(window);
}

// Reinterprets the CoreWindow into an ICoreWindow, which will set the window to pull events from.
void __cdecl Keyboard::SetWindow(Windows::UI::Core::CoreWindow^ window)
{
	SetWindow(reinterpret_cast<ABI::Windows::UI::Core::ICoreWindow*>(window));
}

// Retrieves the current Keyboard state.
Keyboard::State Keyboard::GetState() const
{
	State state;
	p_impl->GetState(state);

	return state;
}

// Resets the Keyboard State.
void Keyboard::Reset()
{
	p_impl->Reset();
}

// Whether or not the Keyboard is detected / connected.
bool Keyboard::IsConnected() const
{
	return p_impl->IsConnected();
}


